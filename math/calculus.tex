\section{Calculus}
\label{sec:calculus}
\epigraph{``God does not care about our mathematical difficulties -- he
integrates empirically.''}{--- \textup{Albert Einstein (1879 -- 1955)}}

\subsection{Limits}
\subsubsection{Zeno's Paradoxes}
In this section we will consider the fundamental concept of
Calculus, the \textit{limit}. We will start by considering
one of ``Zeno's paradoxes'' from ancient Greece. 

``Achilles and the Tortoise'' is a famous paradox created by
Zeno of Elea (Greek: \textgreek{Z'inon o Ele'atis};
490 BC - 430 BC), that proves it is impossible for Achilles
(Greek: \textgreek{'Achill'efs}) who is the speediest runner
in Greek to win a race against the slowest runner (the 
Tortoise), if the Tortoise is given ahead start. Here's how
it goes. \\[1ex]

\textit{Achilles allows the tortoise a head start of 100 meters, for example. If we suppose that each racer starts running at some constant speed (one very fast and one very slow), then after some finite time, Achilles will have run 100 meters, bringing him to the tortoise's starting point. During this time, the tortoise has run a much shorter distance, say, 10 meters. It will then take Achilles some further time to run that distance, by which time the tortoise will have advanced farther; and then more time still to reach this third point, while the tortoise moves ahead. Thus, whenever Achilles reaches somewhere the tortoise has been, he still has farther to go. There are an infinite number of points that Achilles must reach where the tortoise has already been. Therefore, he can never overtake the tortoise!!!} \cite{AchillesTortoise} \\[1ex]

Now, we know that this result is absurd. Yet, the great thing
about this paradox -- the reason that people still talking
about it 2,500 years later -- is it's hard to figure out what
is wrong with Zeno's reasoning.

Let's use algebra to solve this problem. Set the speed of
Achilles as $v_{ach}$, the speed of the Tortoise as $v_{tor}$,
the distance between Achilles and the Tortoise as $d$, and
we want to solve $t$, which is the time that Achilles meets
the Tortoise.

\begin{align*}
    d &= vt \quad t = \frac{d}{v} \\
    t &= \frac{d}{v_{ach}-v_{tor}}
\end{align*}

Achilles will catch the Tortoise in a positive finite time,
since $v_{ach}-v_{tor} > 0$ and $d > 0$. However, there are
infinite amount of intervals that Achilles has to reach before
catching the Tortoise. The length of the $n$th interval is
$d \cdot (\frac{v_{tor}}{v_{ach}})^n$. Therefore, we can say
that

\begin{equation*}
    \lim_{n\to\infty} d \cdot (\frac{v_{tor}}{v_{ach}})^n = 0
\end{equation*}

\subsubsection{The Definition of a Limit}
In this section, we will apply what we have learned from Zeno's
paradox to arrive at a definition of a limit. The concept of a
limit can be defined in a mathematically rigorous was that allows
us to prove that certain limits exist and have unique values. Yet,
more important, it gives us the foundation to prove rigorous
\textit{Limit Theorems} that hold for \textit{abstract functions},
not just the well-behaved functions that we are familiar so far.

Keeping with the big picture for now, remember that in Zeno's
paradox that snapshots of Achilles ant the Tortoise are coordinated.
They hold some relationship which construct a plausible season
that Achilles will alway behind the Tortoise in a finite amount
of processes. Naturally, we know that Achilles indeed can catch up
with the Tortoise in a finite time and distance. Therefore, we
can conclude that the process must take place infinite times, and
somehow the sum of the infinite snapshots of distances must be finite.
We tried to gather this in the language of limits, but struggling
with finding the components that is able to enter the definition.
This is hard even for the people who use the word ``limits'' daily.
A good starting point, though, is to think about limits based on your
previous experience. One of the elements that frequently appears in
people's answer about the definition of limit is a statement like
``as $x$ approaches $a$, $f(x)$ approaches $L$.''  \\[1ex]

\textit{
    Like when you put your finger near a flame, as your finger approaches
    the flame, the temperature increases. The temperature function
    about your finger's position $x$ is $f_{t}(x)$, and the flame is at
    point $a$. The temperature that your finger experiences will get closer
    and closer to the flame's temperature $L=f_{t}(a)$ as it moves towards
    the flame.
}\\[1ex]

Zeno took the contest between Achilles and the Tortoise and broke it into
an infinite amount of intervals, when analyzing the concept of ``motion''.
We can use the same analysis with limits. Instead of defining limits in
terms of unspecified motions, build them around an infinite set of different,
but correlated, degree of closeness to $L$. This obviates the need to define
``motion'', although we need to fill in more details about how to relate the
closeness of $x$ to $a$ to the closeness of $f(x)$ to $L$.

Our example with $\lim_{n\to\infty} d \cdot (\frac{v_{tor}}{v_{ach}})^n$
shows that it was not enough that, for large enough $n$, $(\frac{v_{tor}}{v_{ach}})^n$
was \textit{close} to 0; we had to be able to find values of $n$ for which
it was \textit{infinitely close} to $0$. We have to build these points that
are infinitely close to $0$ into the process from the beginning. Thus, when
we speak of $x$ being close to $a$, it is natural to consider a set of number
$N(a)$, then all numbers between $x$ and $a$ are in it too. This type of set
is called a ``neighborhood''.

By using these neighborhoods $N(a)$, we guarantee that the numbers closest to
$a$ are always being taken into consideration, and that lessens the necessity
of focusing on the exact distance from $x$ to $a$. Now a set containing all
the real numbers between two given numbers is just an open interval. Thus we
start with the following definition.

\theoremstyle{definition}
\begin{definition}
For any real number $a$, we define a ``neighborhood
of $a$'' as an open interval of the real numbers that contains $a$. For example,
if $a$ and $h$ are real numbers, with $h>0$, then the open interval
$(a-h, a+h)$ is a neighborhood of $a$, denoted by $N(a,h)$.
\end{definition}

We can anticipate that we will also wish to consider a set of numbers
\textit{close} to $a$ but not \textit{equal} to it. This is an important
type of neighborhood called a ``deleted neighborhood''.

\theoremstyle{definition}
\begin{definition}
If $N(a,h)$ is a neighborhood of $a$, then the set
\end{definition}

\begin{equation*}
    N^{-}(a,h) = N(a,h) - {a},
\end{equation*}

that is, the set $N$ with $a$ itself excluded, is called a ``deleted
neighborhood'' of $a$. We have

\begin{equation*}
    N^{-}(a,h) = (a-h,a) \cup (a,a+h).
\end{equation*}

Notice that it is possible pick values of $e$ large enough so that you
can find values of $d$ that work -- i.e. that  $f(x) \in N(L, e)$ whenever
$x \in N(a, d)$. But, unless you can do this for \textit{all} values of
$e$, the value $L$ cannot be the correct limit.

Now we are ready to update our limit definition to take into account our
latest investigation. Remember that if $L$ is \textit{not} the limit of
$f(x)$ as $x\to a$, then

\begin{enumerate}
    \item There is at least one value of $e>0$ suck that the following is
    true.
    \item It is possible to find a value of $x$ in \textit{any} neighborhood
    of $a$, $N(a,d)$, such that $f(x)\not\in N(L, e)$.
\end{enumerate}

Then we can write the definition of limit in several equivalent ways.\\[1ex]

First:

\begin{enumerate}
    \item For all values of $e>0$, the following is true.
    \item There is some neighborhood of $a$, $N(a,d)$, such that it is not
    possible to find an $x\in N(a,d)$ with $f(x) \not\in N(L,e)$.
\end{enumerate}

Second:

\begin{enumerate}
    \item For all values of $e>0$, the following is true.
    \item There is some neighborhood of $a$, $N(a,d)$, such that for every
    $x\in N(a,d)$, we have $f(x)\in N(L,e)$.
\end{enumerate}

Third:

\begin{enumerate}
    \item For all values of $e>0$, the following is true.
    \item There is some value of $d>0$, suck that, for every $x\in N(a,d)$,
    we have $f(x)\in N(L,e)$.
\end{enumerate}

Just to be clear: the last definition above says that each value of $e$ has
some value of $d$ that meets the condition. It dose \textit{not} say that there
is only one value of $d$ that works for \textit{all} values of $e$. For this
reason, we usually view $d$ as a \textit{function} of $e$, and write it as $d(e)$.

Now we can write our real definition of a limit:\\[1ex]

``If, for any positive $e>0$, there exists a function $d(e)>0$ such that whenever
$x\in N(a, d(e))$, we have $f(x)\in N(L,e)$, then we say that the limit $\lim_{x\to a}
f(x)$ exists, and equals $L$.''

\subsubsection{Properties of Neighborhoods}

In this section, we will consider more types of neighborhoods and prove a property
that will be important in limit proofs.

The two halves of a deleted neighborhood are called the \textit{left} and the
\textit{right} deleted neighborhoods of $a$ in the following definition. \\[1ex]

\theoremstyle{definition}
\begin{definition}
If $a$ is a real number, and $h$ a positive real number, then
the interval $N^{-}(a^{-},h) = (a-h,a)$ is called a \textit{left deleted neighborhood}
of $a$, and the interval $N^{-}(a^{+},h)=(a,a+h)$ is called a \textit{right deleted
neighborhood} of $a$.
\end{definition}

We have

\begin{equation*}
    N^{-}(a,h)=N^{-}(a^{-},h)\; \cup\; N^{-}(a^{+},h),
\end{equation*}

or the union of any left deleted neighborhood of $a$ with its right counterpart is
itself a deleted neighborhood of $a$.\\[1ex]

\theoremstyle{definition}
\begin{definition}
A neighborhood $N^{-}_{\infty}(M) = (M, \infty)$ is called a
\textit{deleted neighborhood of infinity}, and the interval $N^{-}_{-\infty}(M)=
(M, \infty)$ is called a \textit{deleted neighborhood of negative infinity}.
\end{definition}

Finally, we have the following definition.

\theoremstyle{definition}
\begin{definition}
A neighborhood $N(a,h) = N(a;h,h)$ of $a$, where $h=k$, is called a ``
symmetric neighborhood''.
\end{definition}

We have

\begin{equation*}
    N(a,h) = (a-h, a+h) = \{ x\in\rm I\!R\; |\; |x-a|<h \}
\end{equation*}

The deleted equivalent is

\begin{equation*}
    N^{-}(a,h) = (a-h,a)\;\cup\;(a,a+h) = \{ x\in\rm I\!R\; |\; |x-a|<h \}.
\end{equation*}

Below is a summary of the different types of neighborhood and their uses in
limits. The term ``anchor'' means the finite point $a, \infty$, or $-\infty$.

%TO DO

Now we will investigate some more properties of neighborhoods, prepare to prove
an important property of the limit definition. \\[1ex]

\begin{theorem}[Fundamental Property of Neighborhoods] \label{theo:nei}
If $N_{1}$ and $N_{2}$ are any two neighborhoods of the same type and have the
same anchor, then their intersection $N_{1}\;\cap\;N_{2}$ is also the same
type and has the same anchor.
\end{theorem}

\begin{proof}
\begin{enumerate}
    \item \label{1}
    Let $N_{1}(a;h_{1},k_{1})$ and $N_{2}(a;h_{2},k_{1})$ be two two-sided
    neighborhoods of $a$, and let $h=\min(h_{1},h{2})>0$ and $k=\min(k_{1},k_{2})>0$.
    These are both positive because, by the definition of neighborhoods, $h_1$,
    $h_2$, $k_1$ and $k_2$ are all positive. The intersection of the two neighborhoods
    is $N(a;h,k)=(a-h,a+k)$. Since $a$ is in this interval, and $h$ and $k$ are $>0$,
    and $N(a;h,k)$ is an open interval of the real numbers; it is a neighborhood
    of the same anchor $a$, too, by definition.

    \item Let $N_{1}^{-}(a;h_{1},k_{1})$ and $N_{2}^{-}(a;h_{2},k_{1})$ be two
    two-sided deleted neighborhoods of $a$. They are formed by excluding $a$ from
    the neighborhoods $N_{1}(a;h_{1},k_{1})$ and $N_{2}(a;h_{2},k_{1})$, respectively.
    Since $a$ is in neither $N_{1}^{-}$ nor $N_{2}^{-}$, it will not be in their
    intersection. Thus, we can get the intersection of $N_{1}^{-}$ and $N_{2}^{-}$
    by intersecting the undeleted neighborhoods $N_{1}$ and $N_{2}$, and then excluding
    $a$. The latter intersection is, by case (\cref{1}) above, an undeleted neighborhood
    of $a$; thus, when we exclude $a$, we get that $N_{1}^{-}\;\cap\;N_{2}^{-}$ is
    a deleted neighborhood of the same anchor $a$.

    \item Let $N_{1}^{-}(a^-,h_1)=(a-h_1, a)$ and $N_{1}^{-}(a^-,h_2)=(a-h_2, a)$
    be two left deleted neighborhoods of $a$. Then $a_1, h_2 > 0$, and thus $h=\min
    (h_1,h_2)>0,\;N_{1}^{-}(a^-,h_1)\cap N_{2}^{-}(a^-,h_2)=(a-h_1,a)\cap(a-h_2,a)
    =(a-h,a)$, which is also a left deleted neighborhood with the same anchor $a$.

    \item Let $N_{1}^{-}(a^+,k_1)=(a,a+k_1)$ and $N_{1}^{-}(a^+,k_2)=(a-k_2, a)$
    be two right deleted neighborhoods of $a$. Then, $k_1,k_2>0$, and since $k=\min
    (k_1,k_2)>0$, $N(k_1,k_2)>0,N_{1}^{-}(a^+,k_1)\cap N_{1}^{-}(a^+,k_2)=(a,a+k)$,
    which is also a right deleted neighborhood with the same anchor $a$.

    \item Let $(N_{\infty}^{-})_{1}=(m_1,\infty)$ and $(N_{\infty}^{-})_{2}=(m_2,
    \infty)$ be two deleted neighborhoods of infinity. Then, if $m=\max(m_1,m_2),
    (N_{\infty}^{-})_{1}\cap (N_{\infty}^{-})_{2}=(m_1,\infty)\cap (m_2,\infty)$,
    which is also a deleted neighborhood with the same anchor $\infty$.

    \item Let $(N_{-\infty}^{-})_{1}=(-\infty,m_1)$ and $(N_{-\infty}^{-})_{2}=(-
    \infty,m_2)$ be two deleted neighborhoods of negative infinity. Then, if $m=
    \min(m_1,m_2),(N_{-\infty}^{-})_{1}\cap (N_{-\infty}^{-})_{2}=(-\infty,-m_1)
    \cap (-\infty,m_2)$, which is also a deleted neighborhood with the same
    anchor $-\infty$.

    \item Thus, we have shown that, regardless of the type of anchor, the intersection
    of two neighborhoods with the same type and anchor is another (non-empty)
    neighborhood of the same type and anchor.
\end{enumerate}
\end{proof}

The importance of neighborhoods for us in proving the Limit Theorems is that it
provides a unified way to handle all the different types of limits that are in
the table above. If we prove the Limit Theorems using the property of neighborhoods
in the theorem we just proved, then our Limit Theorems will be valid, regardless
of the type of limit and we don't have to revisit these proofs.

The Limit Theorem proofs, however, do not benefit from our use of neighborhoods
to describe the closeness of $f(x)$ to $L$. This is because we need further properties
than exist in our proof. In fact, we will need the Triangle Inequality to deal
with the $y$-values. This means that, although our Limit Theorems can be extended
to limits at infinity, they cannot be extended to infinite limits.

The previous discussion motivates the following definition of limit.

\theoremstyle{definition}
\begin{definition}
    Suppose $f$ is a function defined for values of $x$ near the anchor $a$, which
    can be either finite pr infinite. We say that

    \centerline{The limit of $f(x)$ as $x$ approaches ``$a$'' \underline{exists} and
    equals $L$,}

    and write

    \begin{equation*}
        \lim_{x\to a}f(x)=L,
    \end{equation*}

    if, given any $\varepsilon>0$, there corresponds a deleted neighborhood $N_{\varepsilon}
    ^{-}(a)$ of the anchor $a$, such that

    \begin{equation*}
        L - \varepsilon < f(x) < L + \varepsilon
    \end{equation*}

    whenever $x$ is in $N_{\varepsilon}^{-}(a)$ and in the domain $D_{f}$ of the function
    $f$.
\end{definition}
In this definition we have addressed several questions raised at the beginning of our
discussion above.

\begin{enumerate}
    \item We bow to convention, and use $\varepsilon$ instead of $E$ (derived from ``error'').
    Later, we will use $\delta$ instead of $D$ (derived from ``distance''). The Latin-latter
    terms are not truly descriptive of the roles these variables play in the definition
    of the limit.

    \item One of the most important classes of functions that we wish to find the limits
    of is limits like $lim_{x\to0}x/x$, all of which have the form $0/0$. In order to extend
    our definition to include such functions, which may not be defined at $a$, or whose
    value at $a$ may be different that the limit, we use a \textit{deleted neighborhood}
    of $a$.

    \item Unlike $x$, though, the $y$ values of the function $f(x)$ can equal $L$, because
    if the function $f(x)$ has no inverse, then the value $L$ may appear multiple times
    as values of $f(x)$ where $x\in N^{-}(a).$ Thus there is no need to use deleted neighborhoods with $y$-values, although, as noted above, we will not use neighborhoods
    for $y$-values ar all.

    \item We chose to write the neighborhood of $L$ as the symmetric neighborhood $(L-
    \varepsilon, L+\varepsilon)$.
\end{enumerate}

Now we are poised to answer an important question: ``are limits unique?'' Does the limit
definition prevent a function from approaching two different limits as $x$ approaches $a$?
After all, the definition says that ``for each $\varepsilon>0$ there corresponds a deleted
neighborhood $N_{\varepsilon}^{-}(a)$ of $a$,'' but it does not say that this deleted
neighborhood is unique. It is in fact easy to see that if one deleted neighborhood of $a$
works in the definition, then any subset of that neighborhood that is also a neighborhood
will also work. Is it somehow possible that one person could use a particular deleted
neighborhood to prove that $\lim_{x\to a}f(x)=L_1$, say, while another person uses a different
neighborhood to prove that $\lim_{x\to a}f(x)=L_2\ne L_1$? The following theorem shows
that this never happens, that is, if a limit exist, it is unique.

This proof uses the Triangle Inequality of the real numbers, which we will prove in the
next section. The Triangle Inequality states that if $x$ and $y$ are any two real numbers,
then

\begin{equation*}
    |x+y| \le |x| + |y|.
\end{equation*}

\begin{theorem}[Uniqueness of Limits]
If $\lim_{x\to a}f(x)=L_1$ and $lim_{x\to a}f(x)=L_2$, then $L_1=L_2$.
\end{theorem}

\begin{proof}
\begin{enumerate}
    \item We use an indirect proof, by contradiction.

    \item Let $f(x)$ be a real function, let $L_1, L_2\in\rm I\!R$ and suppose that $\lim_{
    x\to a}=L_1$ and $\lim_{x\to a}=L_2$.

    \item By the definition of limit, applied to $L_1$, for all $\varepsilon>0$ there is a
    deleted neighborhood $N_{\varepsilon}^{-}(a)_1$ of $a$, such that $|f(x)-L_1|<\varepsilon$
    whenever $x$ is in $N_{\varepsilon}^{-}(a)_1\cap D_f$.

    \item Similarly, there is a deleted neighborhood $N_{\varepsilon}^{-}(a)_2$ of $a$, such
    that $|f(x)-L_2|<\varepsilon$ whenever $x$ is in $N_{\varepsilon}^{-}(a)_2\cap D_f$.

    \item Now, suppose that, contrary to the conclusion of the theorem, $L_1\ne L_2$. Then
    $L_1-L_2\ne 0$ so $|L_1-L_2|>0$, which allows us to use $L=\frac{1}{3}|L_1-L_2|$ as a
    value of $\varepsilon$.

    \item Now let $N_{\varepsilon}^{-}(a)_3=N_{L}^{-}(a)_2\cap N_{L}^{-}(a)_2$. Then by the
    \cref{theo:nei}, $N_{\varepsilon}^{-}(a)_3$ is a deleted neighborhood of $a$.

    \item Then for any $x$ in $N_{\varepsilon}^{-}(a)_3$, $x\in N_{L}^{-}(a)_1$ and $x\in
    N_{L}^{-}(a)_2$ and so both
    \begin{equation*}
        \left[|f(x)-L_{1}<\frac{|L_1-L_2|}{3}|\right] \text{and} \left[|f(x)-L_{2}<\frac{|L_1-L_2|}{3}|\right]
    \end{equation*}
    hold simultaneously.

    \item Then, using the Triangle Inequality, we have
    \begin{align*}
        |L_1-L_2| &= |L_1-f(x)+f(x)-L_2| \\
        &\le |L_1-f(x)| + |f(x)-L_2| \\
        &= |f(x)-L_1| + |f(x)-L_2| \\
        &< \frac{|L_1-L2|}{3} + \frac{|L_1-L2|}{3} = \frac{2|L_1-L2|}{3}
    \end{align*}

    \item Thus $(L_1\ne L_2)\implies (|L_1-L_2|<\frac{2}{3}|L_1-L_2|)$, a contradiction.

    \item Therefore, we conclude that our assumption is false, that $L_1=L_2$ and that
    $\lim_{x\to a}f(x)$, when it exists, is unique.
\end{enumerate}
\end{proof}
