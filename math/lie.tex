\section{Lie Group Theory}
\epigraph{``The universe is an enormous direct product of
  representations of symmetry groups.''}{--- \textup{Hermann Weyl
    (1885 -- 1955)}}

\subsection{Groups}
\textbf{Group Theory} is a framework that deals about symmetries. \\

\textit{Symmetry is defined as an invariance under a set of
  transformations. Thus, we define a group as a collection of
  transformations.} \\

\begin{definition}
A set of transformations $G$ with a binary operation $\circ$ defined
on $G$ is a group, if it satisfies the following axioms.

\begin{itemize}
  \item Closure: For all $g_1, g_2 \in G, g_1 \circ g_2 \in G$. \label{closure}
  \item Identity: There exists an identity element $e \in G$ such that
    for all $g\in G, g\circ e=e\circ g = g$.
  \item Inverses: For each $g\in G$, there exists an inverse element
    $g^{-1}\in G$ such that $g\circ g^{-1} = e = g^{-1}\circ g$.
  \item Associativity: For all $g_1,g_2,g_3\in G, g_1\circ (g_2\circ
    g_3) = (g_1\circ g_2)\circ g_3$.
\end{itemize}
\end{definition}

\subsection{Lie Algebras}
Lie Theory is all about symmetries that can be represented by
infinitesimal transformation, or in other words continuous
symmetries. An example is the continuous symmetry of the rotation of a
circle. Continuous means there are elements in the group which are
arbitrarily close to the identity transformation, namely,
infinitesimal transformations. In contrast, a discrete group has just
a finite number of elements and there for no two elements are close to
the identity, e.g., the symmetry of a rotational square. A
transformation $g$ close to the identity is denoted as

\begin{equation}
  g(\varepsilon) = \underbrace{I}_\text{identity transformation}
  + \varepsilon \underbrace{X}_\text{generator}.
\end{equation}

where $\varepsilon$ is, as always, a very small number. We can make
finite transformations by composing many infinitesimal
transformations, e.g. rotating a circle a tiny bit many times

\begin{equation}
  h(\theta) = (I+\varepsilon X)(I+\varepsilon X) \cdots
  = (I+\varepsilon X)^k,
\end{equation}

where $k$ denotes how many times we repeat the infinitesimal
transformation. We can make $\varepsilon = \theta/N$, so we have

\begin{equation}
  h(\theta) = \lim_{N\to\infty}(I + \frac{\theta}{N}X)^n = e^{\theta X}.
\end{equation}

The reason that $X$ is called a generator is, in some sense the object $X$
generates the finite transformation $h$. We can look at this from another
perspective. If we consider a continuous group of transformations that are
given by matrices, we can make a Taylor expansion of a finite transformation in
the group about the identity.

\begin{equation}
  h(\theta) = \sum_{n=0}^{\infty}
  \frac{\frac{d^nh}{d\theta^n}}
  {n!}\bigg\vert_{\theta=0}\theta^n
  = \exp\frac{d^nh}{d\theta^n}\theta\bigg\vert_{\theta=0}.
\end{equation}

We get that

\begin{equation} \label{eq:gen}
  X = \frac{dh}{d\theta}\vert_{\theta=0}.
\end{equation}

The idea behind such lines of thought is that we can learn a lot about the
group by looking at the important part of the infinitesimal elements: the
\textbf{generators}. After all, we can construct all finite transformations in
a Lie group from its infinitesimal transformations.

We can define Lie algebra from its corresponding matrices Lie group.\\

\textit{For a Lie group G given by $n\times n$ matrices, the Lie algebra
$\mathfrak{g}$ of G is given by those $n\times n$ matrices $X$ such that
$e^{tX}\in G$ for $t \in \mathbb{R}$.}\\

We can use the binary operation $\circ$ to combine two group elements. Such
operation is just an ordinary matrix multiplication for matrix Lie group, but
the combination of two elements in Lie algebra is not that simple. We have to
solve for the equation

\begin{equation}
  Z = \log(e^X \circ e^Y).
\end{equation}

The solution of the above equation is called the Baker--Campbell--Hausdorff
formula, which is

\begin{equation}
  \underbrace{e^X}_{\in G}\circ \underbrace{e^Y}_{\in G}
  = \underbrace{e^{X+Y+\frac{1}{2}[X,Y] + \frac{1}{12}
  \left([X,[X,Y]]+[Y,[Y,X]]\right)
  - \frac{1}{24}[Y,[X,[X,Y]]]\cdots}}_{\in G}
\end{equation}

where $X,Y\in\mathfrak{g}$. The symbol $[,]$ is called the \textbf{Lie bracket}
and for matrix Lie groups it is calculated by $[X,Y] = XY - YX$, which is
called the commutator of $X$ and $Y$. Note that the elements $XY$ and $YX$ are
not necessarily in the Lie algebra, but their difference is. For group elements
$g, h \in G$ we always have $g\circ h \in G$ by the algebraic closure
\cref{closure} of a group. However, for elements in Lie algebra $X,Y\in
\mathfrak{g}$, we have $[X,Y]\in \mathfrak{g}$ and in general $X\circ Y\notin
\mathfrak{g}$. Thus, Lie algebra is \textbf{closed} under the Lie bracket $[,]$, like group elements are closed under $\circ$.

\subsection{Lie Algebra of SO(3)}
The defining conditions of the $SO(3)$ group are

\begin{equation}
  O^TO = I \quad \text{and} \quad \det(O) = 1.
\end{equation}

We can represent every group element $O$ in terms of a generator $J$:

\begin{align}
  O &= e^{\Phi J} \nonumber\\
  O^TO = (e^{\Phi J})^Te^{\Phi J} = e^{\Phi J^T}e^{\Phi J}
  = I &\implies J^T + J = 0_{3,3} \\
  \det(O) = 1 &\implies \det(e^{\Phi J}) = e^{\Phi \tr(J)} = 1 \nonumber\\
  \tr(J) &= 0.
\end{align}

There are three linearly independent matrices satisfy the above conditions.

\begin{equation}
  J_{\mathbf{1}}={\begin{bmatrix}0&0&0\\0&0&-1\\0&1&0\end{bmatrix}},\quad
  J_{\mathbf{2}}={\begin{bmatrix}0&0&1\\0&0&0\\-1&0&0\end{bmatrix}},\quad
  J_{\mathbf{3}}={\begin{bmatrix}0&-1&0\\1&0&0\\0&0&0\end{bmatrix}}
\end{equation}

They are a \textbf{basis} for the generators of the group $SO(3)$. We can also
write the basis of $SO(3)$ generators in terms of the Levi-Civita symbol
$\epsilon$

\begin{equation}
  (J_i)_{jk} = -\epsilon_{ijk}.
\end{equation}

One can show that

\begin{align}
  R_m &= e^{\Phi j_m} = \sum_{n=0}^\infty \frac{\Phi^n j_m^n}{n!} \nonumber \\
      &= \cos(\Phi) I + \sin(\Phi) j_m,
\end{align}

easily, so it is obvious that they are the well-known rotation matrices in
3-dimensions, which are

\begin{equation}
  R_{1}(\theta)=\begin{bmatrix}
    1&0&0\\
    0&\cos \theta &-\sin \theta \\
    0&\sin \theta &\cos \theta
  \end{bmatrix},
  R_{2}(\theta)=\begin{bmatrix}
    \cos \theta &0&\sin \theta \\
    0&1&0\\
    -\sin \theta &0&\cos \theta
  \end{bmatrix},
  R_{3}(\theta)=\begin{bmatrix}
    \cos \theta &-\sin \theta &0\\
    \sin \theta &\cos \theta &0\\
    0&0&1
  \end{bmatrix}.
\end{equation}

We can compute the corresponding Lie bracket from those generators

\begin{equation}
  [J_i,J_j] = \epsilon_{ijk}J_k,
\end{equation}

where $\epsilon_{ijk}$ is the Levi-Civita symbol.

In physics it is conventional to make the generators Hermitian, which fulfill
$J^\dagger = (J^*)^T  = J$, so we add an extra $i$ to put them in the complex
domain, that is, we have $e^{i\phi J}$ instead. Thus, our generators are

\begin{equation}
  J_{\mathbf{1}}=i{\begin{bmatrix}0&0&0\\0&0&-1\\0&1&0\end{bmatrix}},\quad
  J_{\mathbf{2}}=i{\begin{bmatrix}0&0&1\\0&0&0\\-1&0&0\end{bmatrix}},\quad
  J_{\mathbf{3}}=i{\begin{bmatrix}0&-1&0\\1&0&0\\0&0&0\end{bmatrix}},
\end{equation}

and the Lie algebra is

\begin{equation}
  [J_i, J_j] = i\epsilon_{ijk}J_k.
\end{equation}

Also, one can show that

\begin{equation}
  J_n = \frac{dR_n}{d\theta}\vert_{\theta=0},
\end{equation}

which is the method to calculated generators from \cref{eq:gen}. However, we do
prefer to use the more general method which shown above, since we cannot always
start will given finite transformation matrices, e.g. the Lorentz group in
Minkowski space.

\subsection{The Abstract Definition of a Lie Algebra}
\begin{definition}
A Lie algebra is a vector space $\mathfrak{g}$ that has a binary operation
$[,]: \mathfrak{g}\times\mathfrak{g}\rightarrow \mathfrak{g}$, ans satisfies
the following axioms:

\begin{itemize}
  \item Bilinearity: $[aX+bY, Z] = a[X,Z] + b[Y,Z]$ and $[Z, aX+bY] = a[Z,X] +
    b[X,Y]$, for arbitrary number $a,b\;\forall\; X,Y,Z \in \mathfrak{g}$.

  \item Anticommutativity: $[X,Y] = -[Y, X] \;\forall\; X,Y\in\mathfrak{g}$.

  \item The Jacobi Identity: $[X,[Y,Z]] + [Z,[X,Y]] + [Y, [Z,X]] = 0
    \;\forall\;X,Y,Z\in \mathfrak{g}$.
\end{itemize}
\end{definition}

Note that there are many other binary operations that fulfill these axioms,
e.g. the Poisson bracket pf classical mechanics. The important point of the
above definition is that it makes no reference to any Lie group. The definition
of a Lie algebra stands on it own.

\subsection{Lie Algebra of $SU(2)$}
$SU(2)$ is a group of $2\times 2$ unitary matrices with unit determinant:

\begin{equation}
  U^\dagger U = UU^\dagger = 1, \quad \det(U) = 1.
\end{equation}

Thus, we have

\begin{equation}
  U^\dagger U = (e^{iJ_k})^\dagger e^{iJ_k} = I, \quad \det(U) = \det(e^{iJ_k}) = 1.
\end{equation}

Using the Baker--Champell--Hausdorf formula and $[J_i, J_i] = 0$.

\begin{align}
  &(e^{iJ_k})^\dagger e^{iJ_k} = e^{-iJ_k^\dagger}e^{iJ_k} = I\nonumber\\
  &\rightarrow e^{-iJ_k^\dagger + iJ_k
  +\frac{1}{2}[J_k^\dagger,J_k^\dagger]+\cdots} = I\nonumber\\
  &J^\dagger_k = J_k.
\end{align}

So the generators of $SU(2)$ must be Hermitian zero-trace matrices, and the
basis is

\begin{equation}
  \sigma_{1}=\sigma_{x}={\begin{pmatrix}0&1\\1&0\end{pmatrix}},\quad
  \sigma_{2}=\sigma_{y}={\begin{pmatrix}0&-i\\i&0\end{pmatrix}},\quad
  \sigma_{3}=\sigma_{z}={\begin{pmatrix}1&0\\0&-1\end{pmatrix}}.
\end{equation}

We can put them into the Lie bracket,

\begin{equation}
  [\sigma_i, \sigma_j] = 2i\epsilon_{ijk}\sigma_k.
\end{equation}

We get rid of the $2$ by defining $J_i = \sigma_i$. Thus, we have

\begin{equation}
  [J_i, J_j] = i\epsilon_{ijk}\sigma_k.
\end{equation}

The Lie bracket results are exactly the same with $SO(2)$, therefore, one can
claim that $SO(3)$ and $SO(2)$ have the same Lie algebra.

\subsection{The Abstract Definition of a Lie Group}
Rotation in 2D spaces is essentially an unit circle since we have $z = a+ib$
and $U(1)$ is defined by $z^*z=1$:

\begin{equation}
  z^*z = (a+ib)^*(a+ib) = (a-ib)(a+ib) = a^2 + b^2 = 1.
\end{equation}

Analogously, $SU(2)$ has a one-to-one map to the unit quaternions, which is
$q=a\mathbf{1} + b\mathbf{i} + c\mathbf{j} + d\mathbf{k}$ and

\begin{equation}
  a^2 + b^2 + c^2 + d^2 = 1.
\end{equation}

It is the same condition that defines a three dimensional sphere object $S^3$,
A.K.A three-sphere.

We observe that a Lie group is a group as well as differentiable manifold, and
the binary group operation $\circ$ is a differentiable map that maps from the
manifold into itself.

Thus, we can conclude that \textbf{there is precisely one
\textit{simply-connected} Lie group for each Lie algebra.}

This simply-connected group is the covering group for all groups other groups
that share the same Lie algebra, because there must be a map tp all other
groups from the simply-connected group, but not vice-versa. For instance,
$SU(2)$ is a double cover of $SO(3)$, because there is a two-to-one map from
$SU(2)$ to $SU(3)$.

In summary,

\begin{itemize}
  \item $S^1 \correspondsto U(1) \leftrightarrow SO(2)$
  \item $S^3 \correspondsto SU(2) \rightarrow SO(3) = \text{half of } S^3$
\end{itemize}

\subsection{Representation Theory}
\textbf{One} group can act on \textbf{many} different kinds of objects. This
idea motivates the development of the representation theory. A representation
is a map between any group element $g$ of a group $G$ and a linear
transformation $R$ of some vector-space $V$

\begin{equation}
  g\rightarrow R(g),
\end{equation}

and the group properties are preserved:

\begin{equation}
  R(e) = I, \quad R(g^{-1}) = R(g)^{-1}, \quad R(g)\circ R(h) = R(g\circ h).
\end{equation}
