# Diary

---

## 2018-01-30
Today, I slept too much. By the time I got up, it is already 11 o'clock. I realized that I should not waste my time like this anymore. I went ahead and installed Google Calendar on my phone to organize my time wisely. One of the reasons that I started to write diary is that it is an opportunity to reflect myself and be aware if I am wasting my time. Another reason is that I can practice my writing skill for natural languages daily. I usually make many grammar errors, and I believe that if practice writing regularly enough, I can reduce my errors drastically. Last by not least, the whole point of this Git repository is to record my experience and knowledge, and not everything is suitable for the formal LaTeX, thus for non-academic or not LaTeX-worthy notes I will cassette them here. That is it for today, way to go!