\section{Basic Classical Mechanics}
\label{sec:classical_mechanics}

\epigraph{``I do not know what I may appear to the world, but to myself I seem
to have been only like a boy playing on the sea-shore, and diverting myself in
now and then finding a smoother pebble or a prettier shell than ordinary,
whilst the great ocean of truth lay all undiscovered before me.''}{---
\textup{Isaac Newton (1643 -- 1727)}}

\subsection{Systems with One Particle}
\subsubsection{Linear motion}
Let's start with Newton's second law of motion. The second law
states that a body's rate of change of momentum, has the same
direction and commensurate with the acting force. The original
Newton's statement is \\[1ex]

\textit{Lex II: Mutationem motus proportionalem esse vi motrici
  impressae, et fieri secundum lineam rectam qua vis illa
  imprimitur.} \cite{law2} \\[1ex]

which translates into \\[1ex]

\textit{Law II: The alteration of motion is ever proportional
  to the motive force impress'd; and is made in the direction
  of the right line in which that force is impress'd.} \\[1ex]

\begin{equation} \label{eq:f_pt}
    \bm{F} = \frac{d\bm{p}}{dt} \equiv \bm{\dot p} = m\bm{a}
\end{equation}

The take-away from the differential equation~\ref{eq:f_pt}
is that the linear momentum has to be conserved (time
independent) when the net force acts on a partial is 0. \\[1ex]

\subsubsection{Rotational Motion}
The angular momentum of the particle about a point $O$, which
denoted by $\bm{L}$ is defined as

\begin{equation} \label{eq:l_rp}
    \bm{L} = \bm{r} \times \bm{p}
\end{equation}

where $\bm{r}$ is the vector from point $O$ to the particle.
Note that the cross product (vector product) is anticommutative,
namely, $\bm{a} \times \bm{b} = -(\bm{b} \times \bm{a})$.
Therefore, the order of $\bm{r}$ and $\bm{p}$ is critical in
the \cref{eq:l_rp}. The definition of the torque $N$ (the
moment of force) about point $O$ is

\begin{equation} \label{eq:n_rf}
    \bm{N} = \bm{r} \times \bm{F} = \bm{r} \times \bm{\dot p}
\end{equation}

Let's try to rewrite the \cref{eq:n_rf} analogous to the
\cref{eq:l_rp}, by the fact that

\begin{equation} \label{eq:n_rf_2}
    \frac{d}{dt}(\bm{r} \times m\bm{v}) = \bm{v}\times m\bm{v}
    + \bm{r} \times \frac{d}{dt}(m\bm{v}) = \bm{r} \times \frac{d}{dt}(m\bm{v}).
\end{equation}

The first term $\bm{v}\times m\bm{v}$ vanishes, because $\bm{v}$
and $m\bm{v}$ are in the same direction. Therefore, \cref{eq:n_rf}
can be rewrite as:

\begin{equation} \label{eq:n_dL}
    N = \frac{d}{dt}(\bm{r} \times m\bm{v}) = \frac{d\bm{L}}{dt} \equiv \bm{\dot L}
\end{equation}

Similar to the \cref{eq:f_pt}, we can conclude that angular momentum
has to be conserved (time independent) when the net torque acts on a partial is 0.

\subsubsection{Work \& Kinetic Energy}
Let's now consider the work done by an external force $\bm{F}$ acts on a particle
from point $a$ to point $b$. The definition of work is

\begin{equation} \label{eq:work_de}
    W_{ab} = \int_{a}^{b} \bm{F}\cdot ds.
\end{equation}

Note that work is a scalar, because of the scalar product (dot product). Assume
the mass of the particle doesn't change, the work done by $\bm{F}$ can be written as:

\begin{equation*}
    \int_{a}^{b} \bm{F}\cdot ds = m \int_{a}^{b} \frac{d\bm{v}}{dt}\cdot \bm{v}\;dt
    = \frac{m}{2} \int_{a}^{b} \frac{d}{dt}(v^2)\;dt
\end{equation*}

Consequently,

\begin{equation} \label{eq:W_vv}
    W_{ab} = \frac{m}{2}\left(v_{b}^2 - v_{a}^2\right)
\end{equation}

The kinetic energy of a particle is $\frac{mv^2}{2}$, and it can be denoted by $T$.
We can rewrite the \cref{eq:W_vv} as:

\begin{equation} \label{eq:W_TT}
     W_{ab} = \frac{m}{2}v_{b}^2 - \frac{m}{2}v_{a}^2 = T_{b} - T_{a}
\end{equation}

Which means that the work done to a particle by an external force is equal to the
change of the kinetic of the particle.

\subsubsection{Conservative Force}
If a force field satisfies that the work $W_{ab}$ is the same regardless
the path between $a$ and $b$, then the force and the system is \textit{
conservative}. The independence of $W_{ab}$ from path implies that the
work done in a contour must be zero.

\begin{equation} \label{eq:f_ds_0}
    \oint \bm{F} \cdot ds = 0
\end{equation}

Furthermore, according to the gradient theorem, the \cref{eq:f_ds_0}
implies that the force $F$ is equal to the negative of the gradient. Note
that if we add a constant in the potential energy $V$, the force does not
change at all.

\begin{equation} \label{eq:f_-v}
    \bm{F} = -\nabla\,V(r)
\end{equation}

Therefore, for a conservative system, the work done by the force is

\begin{equation} \label{eq:W_VV}
    W_{ab} = V_{a} - V_{b}
\end{equation}

If we combine the \cref{eq:W_VV} and equation~\ref{eq:W_TT},
we will get

\begin{equation}
    T_{1} + V_{1} = T_{2} + V_{2}
\end{equation}

Which means that the total mechanical energy $T+V$ is conserved in a
conservative system.

\subsection{Systems with Multiple Particles}
We went though the situation of systems with one particle in details,
and it has many resemblances to systems with multiple particles. This
small section will be more brief than the last one.

\subsubsection{Linear motion}
\begin{equation} \label{eq:p_FF}
    \dot{\bm{p}}_{i} = \underbrace{\sum_{i}\bm{F}^{ex}_{i}}_{\text{external force}}
    + \underbrace{\sum_{j} \bm{F}_{ji}}_{\text{internal force}}
\end{equation}

where $F_{ji}$ denotes the force acts from the particle $j$ to the
particle $i$. Assume that Newton's third law of motion holds \\[1ex]

\textit{
    Forces exerted by particles on each other are equal and in opposite
    direction.
} \\[1ex]

\begin{equation} \label{eq:F__F}
    \bm{F}_{ab} = -\bm{F}_{ba}
\end{equation}

Therefore,

\begin{equation}
    \frac{d^2}{dt^2}\sum_{i} m\bm{r} = \underbrace{\sum_{i}\bm{F}_{i}^{ex}}_{\bm{F}^{ex}}
    + \underbrace{\sum_{\substack{i, j\\
                                  i\ne j}}
                                  \bm{F}_{ji}}_{0}.
\end{equation}

Let's introduce a new quantity called \textit{center of mass} $M$. Its
position vector $\bm{R}$ is defined as

\begin{equation} \label{}
    \bm{R} = \frac{\sum_{i}m_{i}\bm{r}_{i}}{\sum_{i}{m_{i}}}
    = \frac{\sum_{i}m_{i}\bm{r}_{i}}{M}.
\end{equation}

It gives the ``middle'' of the object in a mass point of view, since
it is weighted by mass. Then, we get

\begin{equation}
    \bm{F}^{ex} = M\frac{d^2\bm{R}}{dt^2}.
\end{equation}

It means that the center of mass of a system will move, as if the total
mass of the system gathers in one point -- namely the the center of mass
coordinate -- is acted up on the total external force $\bm{F}^{ex}$.

\begin{equation} \label{eq:F_0}
    \bm{F}^{ex} = 0 \implies \bm{p} = 0 = \sum_{i}m_{i}\frac{d\bm{r}_{i}}{dt} = \frac{Md\bm{R}}{dt}
\end{equation}

Therefore, the linear momentum is conserved, if net external force is 0.

\subsubsection{Rotational Motion} Extand the \cref{eq:F_0} to $\bm{L}$
and $\bm{r}$

\begin{equation}
    \bm{L} = \sum_{i}\bm{r}_i \times \bm{p}_{i} \implies \bm{\dot{L}} = \sum_{i}\bm{r}_i \times \bm{\dot{p}}_{i}
\end{equation}

\begin{equation}
    \bm{\dot{L}} = \sum_{i}\bm{r}_i \times \bm{F}_{i}^{(ex)}
    + \sum_{\substack{i, j\\
                    i\ne j}}
                    \bm{r}_i \times \bm{F}_{ji}
\end{equation}

We can simplify the last term by

\begin{equation}
    \bm{r}_i \times \bm{F}_{ji} + \bm{r}_j \times \bm{F}_{ij} = \left(\bm{r}_i - \bm{r}_j\right) \times \bm{F}_{ji}
\end{equation}

We then get

\begin{equation}
    \bm{\dot{L}} = \bm{\tau}^{(ex)}
    + \underbrace{\frac{1}{2}\sum_{\substack{i, j\\
                    i\ne j}}
                    \bm{r}_{ij} \times \bm{F}_{ji}}_{0} \quad \text{where }\bm{r}_{ij} = \bm{r}_{i} - \bm{r}_{j}
\end{equation}

In conclusion,

\begin{equation}
    \frac{d\bm{L}}{dt} = \bm{\tau}^{(ex)}
\end{equation}
