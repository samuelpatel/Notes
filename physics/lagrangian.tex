% physics.tex
\section{From Newtonian to Lagrangian Mechanics}

\epigraph{``We have already various treatises on Mechanics, but the plan of
this one is entirely new... No diagrams will be found in this work. The methods
that I explain require neither geometrical, nor mechanical, constructions or
reasoning, but only algebraical operations in accordance with regular and
uniform procedure.''}{--- \textup{Joseph-Louis Lagrange (1736 -- 1813)}}

\subsection{Newtonian Mechanics}

Newton's second law of motion is

\begin{equation}
  F = ma
\end{equation}

and we consider a particle is moving in $\mathbb{R}^n$. Let $F$ be a vector
field on $\mathbb{R}^n$ called the \textbf{force}. The particle's position is
$q$ and it depends on time $t \in \mathbb{R}$, it defines a function

\begin{equation}
  q: \mathbb{R} \longrightarrow \mathbb{R}^n.
\end{equation}

We also define velocity and acceleration from $q$, which are

\begin{align*}
  v &= \dot{q} :  \mathbb{R} \longrightarrow \mathbb{R}^n\\
  a &= \ddot{q} : \mathbb{R} \longrightarrow \mathbb{R}^n.
\end{align*}

We can rewrite Newton's law as

\begin{equation}
  ma(t) = F(q(t)).
\end{equation}

This is a second order ordinary differential equation for $q: \mathbb{R}
\longrightarrow \mathbb{R}^n$ which if the vector field $F$ is smooth and has an
upper bound, also $q(t_{0})$ and $\dot{q}(t_{0})$ are given, then its solution
is unique.

We can define \textbf{kinetic energy} as

\begin{equation}
  K(t) := \frac{1}{2}m v(t)\cdot v(t).
\end{equation}

The kinetic energy goes up when a force applies to an object in the direction of
its velocity, because

\begin{align*}
  \frac{d}{dt}K(t) &= ma(t)\cdot v(t) \\
                   &= F(q(t)) \cdot \dot{q}(t)
\end{align*}

Moreover, we can write the above equation in the integral form

\begin{align*}
  K(t_{1}) - K(t_{0}) = \int_{t0}^{t1}F(q(t)) \cdot \dot{q}(t) \,dt.
\end{align*}

Therefore, the change of kinetic energy is equal to the \textbf{work} done by
the force, that is, the integral of $F$ along the curve $q: [t_{0}, t_{1}]
\longrightarrow \mathbb{R}^n$. Recall that a line integral is

\begin{equation*}
  \int _{C}\bm {F} (\bm {r} )\cdot \,d\mathbf {r} =\int _{a}^{b}\mathbf {F} (\mathbf {r} (t))\cdot \mathbf {r} '(t)\,dt.
\end{equation*}

Stokes' theorem relates the surface integral with the line integral in 3
dimension, which is

\begin{equation}
  \iint _{\Sigma }\nabla \times \bm {F} \cdot d{\boldsymbol {\Sigma }}=\oint _{\partial \Sigma }\bm {F} \cdot d\mathbf {r}.
\end{equation}

Thus, it is obvious that the change of kinetic energy is independent from the
path, if and only if

\begin{equation} \label{eq:f=0}
  \nabla \times F = 0,
\end{equation}

Note that

\begin{align*}
  \nabla f(x,y,z) &= \left(\pdv{f}{x}(x,y,z),\pdv{f}{y}(x,y,z),\pdv{f}{z}(x,y,z)\right)\\
  \curl \nabla f &=   \begin{vmatrix}
    \bm {i}  &\bm {j}  &\mathbf {k} \\
    \pdv{}{x} &\pdv{}{y} & \pdv{}{z}\\
    \pdv{f}{x}(x,y,z) & \pdv{f}{y}(x,y,z) & \pdv{f}{z}(x,y,z)
  \end{vmatrix}\\
  &=\left(\pdv{f}{y}{z} - \pdv{f}{z}{y},
  \pdv{f}{z}{x} - \pdv{f}{x}{z},
  \pdv{f}{x}{y} - \pdv{f}{y}{x}\right)\\
  &= \vec{0}.
\end{align*}

Thus, \cref{eq:f=0} implies

\begin{equation}
  F = -\nabla V
\end{equation}

for some function $V: \mathbb{R}^n \longrightarrow \mathbb{R}$. In fact, this
conclusion is also true in higher dimension by a more general version of Stokes'
theorem. In addition, this function is unique up to an additive constant, e.g.

\begin{equation*}
    V \equiv V + \underbrace{C}_\text{constant}.
\end{equation*}

A force with this property is named \textbf{conservative}. In this case, we can
define the \textbf{total energy} and it is conserved

\begin{equation}
  E(t) := K(t) + V(q(t)),
\end{equation}

where $V(q(t))$ is called the \textbf{potential energy} of the particle. We can
show that $E(t)$ is \textbf{conserved}, that is, the function is independent
from time.

\begin{align*}
  \frac{d}{dt}[K(t) + V(q(t))] &= F(q(t)) \cdot \dot{q}(t) + \nabla V(q(t)) \cdot \dot{q}(t)\\
  &= -\nabla V(q(t)) \cdot \dot{q}(t) + \nabla V(q(t)) \cdot \dot{q}(t)\\
  &= 0
\end{align*}

\subsection{Lagrangian Mechanics}

In the Lagrangian approach we define the quantity

\begin{equation}
  L := K(t) - V(q(t))
\end{equation}

to be the Lagrangian, and for any curve $q: [t_{0}, t_{1}] \longrightarrow
\mathbb{R}^n$, we define the \textbf{action} to be

\begin{equation}
  S(q) := \int_{t_{0}}^{t_{1}} L(t) \, dt.
\end{equation}

We can show that the particles follow paths that are ``critical points'' of the
action $S(q)$ functional if and only if $F = ma$ holds. If $q$ is the desired
curve, then the critical points of $S$ is

\begin{equation}
  \frac{d}{ds}S(q_{s})\bigg{\rvert}_{s=0} = 0 \quad \text{where} \quad q_{s} = q + s \cdot \delta q.
\end{equation}

The perturbation function $\delta q: [t_{0}, t_{1}] \longrightarrow \mathbb{R}^n$
has a boundary condition of

\begin{equation*}
  \delta q(t_{0}) = \delta q(t_{1}) = 0.
\end{equation*}

Thus we have

\begin{align*}
  \frac{d}{ds} S(q_{s})\bigg\rvert_{s=0} &= \frac{d}{ds} \int_{t_{0}}^{t_{1}} \frac{1}{2}m \dot{q}_{s}(t) \cdot \dot{q}_{s}(t) - V(q_{s}(t))\,dt\bigg\vert_{s=0} \\
  &= \int_{t_{0}}^{t_{1}} \frac{d}{ds} \left[ \frac{1}{2}m \dot{q}_{s}(t) \cdot \dot{q}_{s}(t) - V(q_{s}(t))\right] \,dt\bigg\vert_{s=0} \\
  &= \int_{t_{0}}^{t_{1}} \left[ m \dot{q}_{s}(t) \cdot \frac{d}{ds}\dot{q}_{s}(t) - \nabla V(q_{s}(t)) \cdot \frac{d}{ds}q_{s}(t)\right] \,dt\bigg\vert_{s=0}
\end{align*}

Next note that

\begin{align*}
  \frac{d}{ds}q_{s}(t) = \frac{d}{ds} \left[ q + s \cdot \delta q \right] = \delta q
\end{align*}

so

\begin{align*}
  \frac{d}{ds}\dot{q}_{s}(t) = \frac{d}{ds}\frac{d}{dt} q_{s}(t) = \frac{d}{dt}\frac{d}{ds} q_{s}(t) = \frac{d}{dt} \delta q(t).
\end{align*}

Thus we have
\begin{align*}
  \frac{d}{ds} S(q_{s})\bigg\rvert_{s=0} &= \int_{t_{0}}^{t_{1}} \left[ m \dot{q}_{s}(t) \cdot \frac{d}{dt}\delta q(t) - \nabla V(q_{s}(t)) \cdot \delta q(t)\right] \,dt \\
  &= \int_{t_{0}}^{t_{1}} m \dot{q}_{s}(t) \cdot \frac{d}{dt}\delta q(t) \,dt - \int_{t_{0}}^{t_{1}} \nabla V(q_{s}(t)) \cdot \delta q(t) \,dt \\
  &= \dot{q}_{s}(t)\cdot \delta q \bigg\rvert_{t_{0}}^{t_{1}} - \int_{t_{0}}^{t_{1}} m \ddot{q}_{s}(t) \cdot \delta q(t) \,dt - \int_{t_{0}}^{t_{1}} \nabla V(q_{s}(t)) \cdot \delta q(t) \,dt \\
  &= -\int_{t_{0}}^{t_{1}} \left[m \ddot{q}_{s}(t) + \nabla V(q_{s}(t))\right] \cdot \delta q(t) \,dt.
\end{align*}

It is obvious that

\begin{equation*}
  \frac{d}{ds} S(q_{s})\bigg\rvert_{s=0}=0 \implies m \ddot{q}_{s}(t) + \nabla V(q_{s}(t)) = 0
\end{equation*}

Therefore, the curve $q$ is a critical point of the action $S$ if and only if

\begin{equation*}
  F = ma.
\end{equation*}

The above conclusion only applies when the forces can be written as minus the
gradient of some potential. Thus, it cannot be applied to charged particles in a
magnetic field.

\subsection{Lagrangian versus Hamiltonian Approaches}
Hamiltonian mechanics focuses on the energy while Lagrangian mechanics focuses
on velocity and position of an object. The key in Lagrangian mechanics is to
understand the integral of the Lagrangian over time, the ``action'' $S$. We can
also peek ahead to quantum mechanics, the quantity $\exp(\frac{iS}{\hbar})$ will
describe the ``change in phase'' of a \textit{quantum} system as the partial
traces out its path. In short, while Lagrangian approach takes a while to build
up, it provides insights into classical mechanics and its relation to quantum
mechanics.

\subsection{Virtual Work}
The \textbf{virtual work} is $\bm{F}_{i}\cdot \delta \bm{r}_{i}$, where
$\delta \bm{r}_{i}$ is \textit{for each position coordinate, and yet remain
consistent with all the constraints and forces at a given instant of time $t$}.
If a system in the special state of being in equilibrium, i.e. when $\sum
F_{i}=0$, then we can conclude that the virtual work vanishes for an equilibrium
system,

\begin{equation}
  \sum \bm{F}_{i}\cdot \delta \bm{r}_{i} = 0, \quad \text{(when in equilibrium)}.
\end{equation}

$n$ number of particles in $\mathbb{R}^3$ can be treated as a single
quasi-particle in $\mathbb{R}^{3n}$, and if there are constraints, it can move
in some sub-manifold of $\mathbb{R}^{3n}$. Ultimately, we need to study a
particle on an arbitrary \textit{manifold}.

If the force is conservative, i.e. $F=-\nabla V$, then this is also equivalent to

\begin{equation*}
  \nabla V(q_{0}) = 0
\end{equation*}

that is, we have equilibrium at critical points of the potential. The
equilibrium will be \textbf{stable} if $q_{0}$ is a local minimum of the
potential $V$.

In summary, we have

\begin{tabular}{l|l}
Statics                & Dynamics                                 \\ \hline
equilibrium, $F=0$     & $F=ma$                                   \\
potential, $V$         & action, $S=\int_{t_{1}}^{t_{2}}K-V\,dt$     \\
critical points of $V$ & critical points of $S$
\end{tabular}

\subsection{From Virtual Work to the Principle of Least Action}
According to the principle of virtual work for the statics the equilibrium occurs when

\begin{equation*}
  F(q_{0}) \cdot \delta q = 0, \quad \forall\, \delta q \in \mathbb{R}^n
\end{equation*}

We can generalize this principle to dynamics by using a concept called the
``inertia force'', $ma$, and the dynamics equilibrium occurs when the so-called
\textbf{total force}, $F-ma$, vanishes. A particle will trace out a path $q:
[t_{0}, t_{1}]\longrightarrow \mathbb{R}^n$ obeying

\begin{equation}
  \left[F(q(t)) - ma(t)\right] \cdot \delta q(t) = 0
\end{equation}

for all $\delta q: [t_{0}, t_{1}]\longrightarrow \mathbb{R}^n$ with the boundary condition

\begin{equation*}
  \delta q(t_{0}) = \delta q(t_{1}) = 0.
\end{equation*}

We create a family of paths parameterized by $s$

\begin{equation*}
  q_{s}(t) = q(t) + s\,\delta q(t)
\end{equation*}

and define the \textbf{variational derivative} of any function $f$ on the space of paths by

\begin{equation}
  \delta f(q) = \frac{d}{ds} f(q_{s})\bigg\rvert_{s=0}.
\end{equation}

The D'Alembert's generalized principle of virtual work implies

\begin{equation*}
  \int_{t_{0}}^{t_{1}} \left[F(q(t))-m\ddot{q}(t)\right]\cdot \delta q(t)\,dt = 0.
\end{equation*}

Thus, if $F=-\nabla V$ we have

\begin{align*}
  0 &= \int_{t_{0}}^{t_{1}} \left[-\nabla V(q(t)) \cdot \delta q(t) - m\ddot{q}(t)\cdot \delta q(t)\right]\\
  &=\int_{t_{0}}^{t_{1}} \left[-\nabla V(q(t)) \cdot \delta q(t) + \underbrace{m\dot{q}(t)\cdot \delta \dot{q}(t)}_\text{integration by parts}\right]
\end{align*}

by integration by part, and applying the boundary condition. Next, using

\begin{align*}
  \delta V(q(t)) = \frac{d}{ds}V(q_{s}(t))\bigg\rvert_{s=0} &= \nabla V(q) \cdot \frac{dq_{s}(t)}{ds}\bigg\rvert_{s=0} = \nabla V(q(t)) \cdot \delta q(t) \\
  \delta(\dot{q}(t)^2) &= 2\dot{q}(t)\cdot \delta \dot{q}(t).
\end{align*}

We obtain

\begin{align*}
  0 &= \int_{t_{0}}^{t_{1}} \left[ -\nabla V(q(t)) \cdot \delta q(t) + m\dot{q}(t) \cdot \delta \dot{q}(t) \right] \, dt \\
  &= \int_{t_{0}}^{t_{1}} \left[ -\delta V(q(t)) + \frac{m}{2}\delta(\dot{q}(t)^2)\right] \, dt \\
  &= \delta\left(\int_{t_{0}}^{t_{1}} \left[ -\delta V(q(t)) + \frac{m}{2}\delta(\dot{q}(t)^2)\right] \, dt\right) \\
  &= \delta\left(\int_{t_{0}}^{t_{1}} \left[ K(t) + V(q(t)) \right] \, dt\right)
\end{align*}

and thus

\begin{equation*}
  \delta S(q) = 0
\end{equation*}

where

\begin{equation*}
  S(q) = \int_{t_{0}}^{t_{1}} \left[K(t) - V(q(t))\right]\, dt
\end{equation*}

is the action of the path $q$.

\section{Lagrangian Mechanics}
In this section, we are going to show that the principle of least action is
equivalent to the Euler-Lagrange equation.

\subsection{The Euler-Lagrange Equation}
We are going to start thinking of a general classical system as a set of points
in an abstract \textit{configuration space} or phase space. An arbitrary
classical system lives in a space of points in some manifold $Q$, e.g. the space
for a double pendulum would be in $Q = S^2 \times S^2$. The system is ``a
particle in $Q$''. Instead of real particles, we are dealing with a
\textit{single abstract particle} in an abstract higher dimensional space. The
system traces out a path

\begin{equation*}
  q: [t_0, t_1] \longrightarrow Q
\end{equation*}

and we define the \textbf{velocity} of the system

\begin{equation*}
  \dot{q} \in T_{q(t)}Q
\end{equation*}

to be the tangent vector at $q(t)$. Let $\Gamma$ be the space of smooth paths
from $a\in Q$ to $b \in Q$,

\begin{equation*}
  \Gamma = \{q: [t_0, t_1] \longrightarrow Q\rvert q(t_0)=a, q(t_1)=b\}
\end{equation*}

Let the \textbf{Lagrangian} for the system be \textit{any} smooth function of
position and velocity (not just $K-V$):

\begin{equation*}
  L: TQ \longrightarrow \mathbb{R}
\end{equation*}

and define the \textit{action}

\begin{equation*}
  S: \Gamma \longrightarrow \mathbb{R}
\end{equation*}

by

\begin{equation}
  S(q) = \int_{t_0}^{t_1} L(q,\dot{q})\,dt.
\end{equation}

The path that the abstract particle will take is a critical point of $S$. In
other words, it will choose a path $q\in \Gamma$ such that for any smooth
1-parameter family of paths $q_s\in\Gamma$ with $q_0=q$, we have

\begin{equation}
  \frac{d}{ds}S(q_s)\bigg\rvert_{s=0} = 0.
\end{equation}

For any function $f$ on the space of paths we define its \textit{variational
derivative} by

\begin{equation*}
  \delta f(q) = \frac{d}{ds} f(q)\bigg\rvert_{s=0}.
\end{equation*}

Thus we have

\begin{equation}
  \delta S(q) = 0.
\end{equation}

1-parameter family of paths is nothing but a set of well defined paths $\{q_s\}$
that labeled by a parameter $s$, e.g.

\begin{equation*}
  q_s(t) = q(t) + s \cdot \delta q
\end{equation*}

Note that it is not the only choice of $\{q_{s}\}$, and physical interpretation
of $q_{s}$ is just $q$ with some perturbations. It offer a way to mathematically
vary a path between $q(t_0)$ and $q(t_1)$.

Since $Q$ is a manifold, it has coordinate charts. Let us pick coordinates in a
neighborhood $U$ of some point $q(t) \in Q$. Next, consider only variations
$q_s$ that satisfy $q_s=q$ outside $U$. We can restrict attention to a
sub-interval $[t'_0, t'_1]\in [t_0, t_1]$ such that $q_{s}(t)\in U$ for $t_0'\le
t\le t_1'$. Let us just rename $t_0'$ and $t_1'$ to $t_0$ and $t_1$. We can use
the coordinate charts on $U$ and $TU$,

\begin{align*}
  \varphi: U &\longrightarrow \mathbb{R}^n\\
  x \mapsto \varphi(x) &= (x^1, x^2, \cdots, x^n)\\
  d\varphi: TU &\longrightarrow T\mathbb{R}^n \equiv \mathbb{R}^n\times \mathbb{R}^n\\
  (x,y) \mapsto d\varphi(x, y) &= (x^1, x^2, \cdots, x^n, y^1, y^2, \cdots, y^n)
\end{align*}

where $y\in T_xQ$. We can restrict $L: TQ\longrightarrow \mathbb{R}$ to $TU\subseteq
TQ$, and then we can describe $L$ using the coordinates $x^i, y^i$ on $TU$. The
$x^i$ are \textit{position} coordinates, and the $y^i$ are the associated
\textit{velocity} coordinates.

\begin{align*}
  \delta S &= \delta \int_{t_0}^{t_1} L(q(t), \dot{q}(t))\, dt\\
  &= \int_{t_0}^{t_1} \delta L(q(t), \dot{q}(t))\, dt\\
  &= \int_{t_0}^{t_1} \underbrace{\pdv{L}{x^i}\delta q^i + \pdv{L}{y^i}\delta \dot{q}^i}_\text{Einstein notation}\, dt
\end{align*}

We can use integration by parts to further simplify the above equation, since
the boundary condition vanishes. We then have

\begin{equation*}
  \delta S = \int_{t_0}^{t_1} \left( \pdv{L}{x^i} - \frac{d}{dt}\pdv{L}{y^i}\right) \delta q^i(t)\, dt = 0
\end{equation*}

It is clear that the term in brackets must be zero,

\begin{equation}
  \pdv{L}{x^i} - \frac{d}{dt}\pdv{L}{y^i} = 0.
\end{equation}

Physicists always give the coordinates $x^i, y^i$ on $TU$ the names $q^i,
\dot{q}^i$ for obvious reasons. Thus, physicists write

\begin{equation*}
  \frac{d}{dt}\pdv{L}{\dot{q}^i} = \pdv{L}{q^i}
\end{equation*}

and they call it the \textbf{Euler-Lagrange equation}. Note that in our
derivation of this equation was fairly abstract: we used the terms ``position''
and ``velocity'', but we did not assume these were the usual notions of position
an velocity for a particle in $\mathbb{R}^3$ or even $\mathbb{R}^n$. Let
consider the simple familiar case where the configuration space $Q$ is
$\mathbb{R}^n$ and the Lagrangian is

\begin{equation*}
  L(q,\dot{q}) = \frac{1}{2}m\dot{q}\cdot \dot{q} - V(q).
\end{equation*}

In this case

\begin{equation*}
  \pdv{L}{q^i} = -\pdv{V}{q^i} = F_i
\end{equation*}

are the components of the force on the particle, while

\begin{equation*}
  \pdv{L}{\dot{q}^i} = m\dot{q}^i
\end{equation*}

are the components of its \textbf{momentum}. We usually denote it by $p$, thus
the Euler-Lagrange equation is simply

\begin{equation*}
  \frac{dp}{dt} = F.
\end{equation*}

It is just another way of stating Newton's second law. Based on this example, we
can define some new terms.

\begin{align*}
  F_i &= \pdv{L}{q^i}\quad \textbf{force} \\
  p_i &= \pdv{L}{\dot{q}^i}\quad \textbf{momentum}
\end{align*}

\subsection{Noether's Theorem}
If the form a system of dynamical equations does not change under spatial
translations then the momentum is conserved. If the form of the equations is
invariant under time translations then the total energy is a conserved quantity.
Both time and space translations are examples of one-parameter groups of
translations, which has a continuous group homomorphism $\varphi :{\mathbb
{R}}\longrightarrow G$. \textit{Invariance under a group of transformations} is
exactly a symmetry in the group theory. Symmetries of a dynamical system give
conserved quantities which forms conservation law. That is the gist of the
Noether's theorem.

\subsubsection{Time Translation}
We need to replace our paths $q: [t_0, t_1] \longrightarrow Q$ by paths $q:
\mathbb{R}\longrightarrow Q$ to handle time translation, which is exactly a
one-parameter group. Define a new space of paths,

\begin{equation*}
  \Gamma = \{q: \mathbb{R} \longrightarrow Q\}.
\end{equation*}

The bad new is that the action then becomes an improper to plus and minus
infinities.

\begin{equation*}
  S(q) = \int_{-\infty}^{\infty} L\left(q(t), \dot{q}(t)\right)\, dt
\end{equation*}

More than likely it will diverge, so $S$ is then no longer a function of space
of paths. Nevertheless, if $\delta q = 0$ outside of some finite interval, then
the function variation,

\begin{equation*}
  \delta S := \int_{-\infty}^{\infty} \frac{d}{ds} L\left(q_s(t), \dot{q}_s(t)\right) \bigg\vert_{s=0} \, dt
\end{equation*}

\textit{will} converge, because the integral is smooth and vanishes outside the
interval.

\begin{align*}
  \delta S &= \int_{-\infty}^{\infty} \frac{d}{ds} L\left(q_s(t), \dot{q}_s(t)\right) \bigg\vert_{s=0} \, dt\\
  &=\int_{-\infty}^{\infty} \left(\pdv{L}{q^i}\delta q^i + \pdv{L}{\dot{q}^i}\delta \dot{q}^i\right) \,dt\\
  &= \int_{-\infty}^{\infty} \left(\pdv{L}{q^i} - \frac{d}{dt}\pdv{L}{\dot{q}^i} \right) \delta q^i \,dt
\end{align*}

again, we use integration by parts and the boundary term vanishes since $\delta
q=0$ vanishes outside an interval, thus $\delta q(+\infty) = \delta q(-\infty) =
0$. To be very explicit,

\begin{align*}
  \pdv{L}{\dot{q}^i}\delta \dot{q}^i &= \frac{d}{dt} \left(\pdv{L}{\dot{q}^i}\delta q^i\right) - \left(\frac{d}{dt}\pdv{L}{\dot{q}^i}\right) \delta q^i\\
  \int_{-\infty}^{\infty}\pdv{L}{\dot{q}^i}\delta \dot{q}^i \,dt &= \int_{-\infty}^{\infty}\frac{d}{dt} \left(\pdv{L}{\dot{q}^i}\delta q^i\right) \,dt - \int_{-\infty}^{\infty}\left(\frac{d}{dt}\pdv{L}{\dot{q}^i}\right) \delta q^i\, dt\\
  \int_{-\infty}^{\infty}\pdv{L}{\dot{q}^i}\delta \dot{q}^i \,dt &= \int_{-\infty}^{\infty}-\left(\frac{d}{dt}\pdv{L}{\dot{q}^i}\right) \delta q^i\, dt\\
\end{align*}

Therefore, we get the Euler-Lagrange equation again.

\subsubsection{Symmetries}
Let us define symmetry transformations in a one-parameter family.
\theoremstyle{definition}
\begin{definition}[one-parameter family of symmetries] \label{def:noether}
  A one-parameter family of symmetries of a Lagrangian system $L: TQ\longrightarrow \mathbb{R}$ is a smooth map,
  \begin{align*}
    F: \mathbb{R}\times \Gamma &\longrightarrow \Gamma\\
    (s, q) &\mapsto q_s, \quad \text{with } q_0 = q
  \end{align*}
  such that there exists a function $\ell(q, \dot{q})$ for which
  \begin{equation*}
    \delta L = \frac{d\ell}{dt}
  \end{equation*}
  for some $\ell: TQ\longrightarrow \mathbb{R}$, that is,
  \begin{align*}
    \frac{d}{ds}L\left( q_s(t), \dot{q}_s(t) \right) \bigg\rvert_{s=0} = \frac{d}{dt}\ell \left(q_{s}(t),\dot{q}_s(t)\right)
  \end{align*}
  for all paths $q$.
\end{definition}
\theoremstyle{remark}
\begin{remark}
  The simplest case is $\delta L = 0$, in which case we really have a way of
moving paths around $(q\mapsto q_s)$ that doesn't change the Lagrangian ---
i.e., a symmetry of $L$ in the most obvious way. But $\delta L = \frac{d}{dt}
\ell$ is sneaky generalization whose usefulness will become clear.
\end{remark}

\subsubsection{Noether's Theorem}
Note that $\ell$ in this theorem is the function associated with $F$ in
\cref{def:noether}.

\theoremstyle{theorem}
\begin{theorem}[Noether's Theorem] \label{the:noether}
  Suppose $F$ is a one-parameter family of symmetries of the Lagrangian system, $L:TQ\longrightarrow \mathbb{R}$. Then,

  \begin{equation*}
    p_i\delta q^i - \ell
  \end{equation*}
  is conserved, that is, its time derivative is zero for any path $q\in \Gamma$ satisfying the Euler-Lagrange equation. In other words,
  \begin{align*}
    \frac{d}{dt}\left[ \pdv{L}{y^i}\left(q(s),\dot{q}(s) \right)
    \frac{d}{ds}q_{s}^{i}(t)\bigg\rvert_{s=0} - \ell(q(t),\dot{q}(t)) \right] = 0.
  \end{align*}
\end{theorem}

\theoremstyle{proof}
\begin{proof} \label{pro:noettherp}
  \begin{align*}
    \frac{d}{dt}\left(p^i\delta q_i - \ell \right) &= \dot{p}^i \delta q_i - \frac{d}{dt}\ell \\
    &= \pdv{L}{q^i} \delta q^i + \pdv{L}{\dot{q}^i}\delta \dot{q}^i - \delta L \\
    &= \delta L - \delta L = 0
  \end{align*}
\end{proof}

\subsection{Conserved Quantities from Symmetries}
\subsubsection{Time Translation Symmetry}
For any Lagrangian system $L:TQ\longrightarrow \mathbb{R}$, we have a one-parameter
family of symmetry

\begin{equation*}
  q_s(t) = q(t+s),
\end{equation*}

because

\begin{align*}
  \delta q_s(t) &= \frac{d}{ds} q(t+s)\bigg\rvert_{s=0} = \dot{q}(t) \\
  \delta \dot{q}_s(t) &= \frac{d}{ds} \dot{q}(t+s)\bigg\rvert_{s=0} = \ddot{q}(t)\\
  \delta L &= \pdv{L}{q_s^i}\delta q_s^i + \pdv{L}{\dot{q_s^i}}\delta \dot{q_s}^i\\
  &= \pdv{L}{q^i}\dot{q} + \pdv{L}{\dot{q}^i}\ddot{q}^i \quad \text{because $q_s$ doesn't change the Lagrangian}\\
  &= \frac{d}{dt}L = \dot{L}\\
  \ell &= L
\end{align*}

Thus, according to Noether's \cref{the:noether},

\begin{equation}
  H = p_i\dot{q}^i - L
\end{equation}

is conserved, and we usually call it the \textbf{total energy} or
\textbf{Hamiltonian}, e.g. a particle on $\mathbb{R}^n$ in a potential $V$ has
$Q=\mathbb{R}^n, L(q,\dot{q}) = \frac{m}{2}\dot{q}\cdot\dot{q} - V(q)$

\begin{align*}
  H = p_i\dot{q}^i - L = m\dot{q}\cdot\dot{q} - L = 2K - (K-V) = K + V.
\end{align*}

\subsubsection{Space Translation Symmetry}
If we have the symmetry

\begin{align*}
  q_s(t) = q(t) + s v, \quad \delta q_s(t) = v
\end{align*}

then it implies that $L$ is independent from $q_s$, and $\delta\dot{q} = 0$ as
the perturbation does not change velocity. We have

\begin{equation*}
  \delta L = 0 = \ell
\end{equation*}

Thus, the \textbf{the momentum in the $v$ direction} is conserved:

\begin{equation*}
  p_i\delta q^i - 0 = p_iv^i.
\end{equation*}

Note that this ``momentum'' is different from the conjugate momentum
$\pdv{L}{\dot{q}^i} = p_i$, because this ``momentum'' is obtained from the
homogeneity nature of space.

\subsubsection{Rotational Symmetry}
We can construct rotational symmetry from special orthogonal group denoted by
$\mathfrak{so}(n)$. If we can transformations by an exponential map $e^{X}$,
then we call $X$ the \textbf{generator}. Rotational transformations satisfy the
condition

\begin{equation*}
  \det R = 1, \quad R^TR = I.
\end{equation*}

We can derivative the generator from there, let $R = e^{X}$

\begin{align*}
  \det e^{X} &= 1 \implies e^{\tr(X)} = 1 \implies \tr(X) = 0\\
  (e^X)^T e^X &= I \implies e^{X^T+X} = I \implies X^T = -X
\end{align*}

Note that we can do $(e^X)^T e^X = e^{X^T+X}$, because $X$ and $X^T$ commute.
Otherwise we have to use the Baker--Campbell--Hausdorff formula. Thus, we can
conclude that $X$ is skew-symmetric and its trace is zero. If we have the
symmetry

\begin{align*}
  q_s(t) = e^{sX}q(t)
\end{align*}

which has

\begin{align*}
  \delta L &= \pdv{L}{q^i}\delta q^i + \pdv{L}{\dot{q}^i}\delta \dot{q}^i = p_i \delta \dot{q}^i
\end{align*}

It's obvious that $\pdv{L}{q^i}=0$, because the transformation does not change
the distance $q_s^i$. Also, $\pdv{L}{\dot{q}^i} = p_i$ in this case, and

\begin{align*}
  \delta \dot{q}^i &= \frac{d}{ds}\dot{q}_s^i \bigg\rvert_{s = 0}\\
  &= \frac{d}{ds}\frac{d}{dt}\left(e^{sX}q\right)\bigg\rvert_{s = 0} \\
  &= \frac{d}{dt}\frac{X}q = X \dot{q}.
\end{align*}

Thus,

\begin{align*}
  \delta L = p_i X_j^i \dot{q}^j = m \dot{q}_i \cdot (X \dot{q}) = 0
\end{align*}

because $X$ is skew-symmetric and thus $ a\cdot Xa = 0 $. We get \textbf{angular
momentum} is conserved in the $X$ direction. If we assume that $X$ is filled by
$\pm{1}$

\begin{align*}
  p_i\delta q^i &= m\dot{q}_i \cdot (X q)^i\\
  &= m(\dot{q}_iq_j - \dot{q}_jq^i)
\end{align*}

In $\mathbb{R}^3$, we have $m\dot{\bm{q}}\times \bm{q}$.

\section{Examples}
\subsection{Free Particles in Special Relativity}
In Minkowski spacetime, we do not parameterize the particle's path by ``time
coordinate''. Minkowski spacetime is,

\begin{equation*}
  \mathbb{R}^{n+1} \ni (x^0, x^1, \cdots, x^n)
\end{equation*}

for $n$-dimensional space. We normally take $x^0$ as ``time'', and $(x^1,
\cdots, x^n)$ as ``space'', but of course this is all relative to one's
reference frame. Someone else traveling at some high velocity relative to us
will have to make a Lorentz translation to translate from our coordinate to
theirs.

Lorentzian metric is

\begin{equation*}
  g(v,w) = \eta^{\mu\nu} v_{\mu} w_{\nu}
\end{equation*}

where

\begin{align*}
  \eta^{\mu\nu} = {\begin{pmatrix}1&0&0&0\\0&-1&0&0\\0&0&-1&0\\0&0&0&-1\end{pmatrix}}.
\end{align*}

In special relativity we take \textit{spacetime} to be the configuration space
of a single point particle, so we let $Q$ be Minkowski spacetime, i.e.,
$\mathbb{R}^{n+1}\ni (x^0, \cdots, x^n)$ with the metric $\eta^{\mu\nu}$ defined
above. Then the path of the particle is,

\begin{align*}
  q: \mathbb{R}(\ni t) \longrightarrow Q
\end{align*}

where $t$ is just an arbitrary parameter for the path, not necessarily $x^0$,
and not necessarily \textit{proper time} either. We want some Lagrangian $L:
TQ\longrightarrow \mathbb{R}$, i.e., $L(q^i, \dot{q}^i)$ such that the
Euler-Lagrangian equations will dictate how our free partial moves at a constant
velocity. Many Lagrangian can do this, but the ``best'' one should give an
action that is \textit{independent} of the parameterization of the path---since
the parameterization is ``unphysical'': it cannot be measured. The action

\begin{equation*}
  S(q) = \int_{t_0}^{t_1} L\left(q^i(t),\dot{q}^i(t)\right)\,dt
\end{equation*}

for $q:[t_0,t-1]\longrightarrow Q$, should be independent from $t$. The obvious
candidate for $S$ is mass times arclength

\begin{equation*}
  S(q) = m\int_{t_0}^{t_1}\sqrt{\eta_{\mu\nu}\dot{q}^\mu(t)\dot{q}^\nu(t)}\, dt
\end{equation*}

it is called the \textbf{proper time}. In Euclidean space, free particles follow
a straight path, so the path length variation is an extremum, and we expect the
same behavior in Minkowski space. The path length does not depend on the choice
of parameterization variable. Thus, we take

\begin{equation} \label{eq:splag}
  L = m \sqrt{\eta_{\mu\nu}\dot{q}^{\mu}\dot{q}^\nu}
\end{equation}

and work out the Euler-Lagrange equations. We have

\begin{align*}
  \dot{p}_{\mu} = \pdv{L}{\dot{q}^\mu} &= m \pdv{}{\dot{q}^\mu}\sqrt{\eta_{\mu\nu}\dot{q}^{\mu}\dot{q}^{\nu}} \\
  &= m \frac{\eta_{\mu\nu}\dot{q}^{\nu}}{\sqrt{\eta_{\mu\nu}\dot{q}^{\mu}\dot{q}^{\nu}}}
  = \frac{m\dot{q}_\mu}{\norm{\dot{q}}}.
\end{align*}

Note that $m$ is there for the sake of the correct unit, and $\dot{p}_{\mu}$
doesn't change, if we change the parameter $\dot{q} \mapsto \alpha \dot{q}$.

\begin{align*}
  \dot{p}_\mu = F_\mu = \pdv{L}{q^\mu} = 0.
\end{align*}

If we parameterize the path by the ``proper time'', the we get

\begin{equation*}
  \int_{t_0}^{t_1}\norm{\dot{q}}\,dt = t_1-t_0, \forall t_0, t_1.
\end{equation*}

Which implies that $\norm{\dot{q}} = 1$, so we get

\begin{align*}
  p_\mu = m&\frac{\dot{q_\mu}}{\norm{\dot{q_\mu}}} = m \dot{q_\mu} \\
  \dot{q_\mu} = 0 &\mapsto m \ddot{q}_\mu = 0.
\end{align*}

Thus, a free particle moves in a straight line without acceleration, as we
expected.

\subsubsection{Gauge Symmetries}
The Lagrangian from \cref{eq:splag} has many symmetries coming from
\textit{reparameterizing the path}, so Noether's theorem yields lots of
conserved quantities for the relativistic free partial. These reparameterization
symmetries work as follows. Consider any smooth one-parameter family of
reparameterizations, i.e., diffeomorphisms

\begin{align*}
  f_s: \mathbb{R}\longrightarrow \mathbb{R}
\end{align*}

with $f_0$ is the identity. These act on the space of path $\Gamma =
{q:\mathbb{R}\longrightarrow Q}$ as follows: given any $q\in \Gamma$ we get

\begin{align*}
  q_s(t) = q(f_s(t))
\end{align*}

where we should note that $q_s$ is \textit{physically indistinguishable} from
$q$. Let us show that

\begin{align*}
  \delta L = \dot{\ell}, \quad \text{(when Euler-Lagrange eqn. holds)}
\end{align*}

so that Noether's theorem gives a conserved quantity

\begin{equation*}
  p_\mu\delta q^\mu - \ell.
\end{equation*}

We have

\begin{align*}
  \delta L &= \pdv{L}{q^\mu} + \pdv{L}{\dot{q}^\mu}\delta \dot{q}^\mu\\
  &= p_\mu \delta \dot{q}^\mu = \frac{m\dot{q}_\mu}{\norm{\dot{q}}} \frac{d}{ds}\dot{q}^\mu(f_s(t))\bigg\rvert_{s=0} \\
  &= \frac{m\dot{q}_\mu}{\norm{\dot{q}}} \frac{d}{dt}\dot{q}^\mu(\frac{df_s(t)}{ds})\bigg\rvert_{s=0} \\
  &= \frac{m\dot{q}_\mu}{\norm{\dot{q}}} \frac{d}{dt}(\dot{q}^\mu \delta f_s) \\
  &= \frac{d}{dt}(p_\mu \dot{q}^\mu \delta f) \\
  \ell &= p_\mu \dot{q}^\mu \delta f
\end{align*}

Note that we have

\begin{equation*}
  \delta q^\mu = \frac{d}{ds} q^\mu(f_s(t))\bigg\rvert_{s=0} = \dot{q}^\mu \delta f.
\end{equation*}

Noether's theorem gives a conserved quantity

\begin{equation*}
  p_\mu\delta q^\mu - \ell = p_\mu\dot{q}^\mu \delta f - p_\mu\dot{q}^\mu \delta f = 0
\end{equation*}

These conserved quantities \textit{vanish}! In short, we're seeing an example of
what physicists call \textbf{gauge symmetries}. Gauge symmetries are

\begin{enumerate}
  \item{These are symmetries that permute different mathematical descriptions of
      the same physical situation, e.g., reparameterizations of a path.}

  \item{These are symmetries make it impossible to compute $q(t)$ given $q(0)$
      and $\dot{q}(0)$: since if $q(t)$ is solution so is $q(f(t))$ for any
        reparameterization $f:\mathbb{R}\longrightarrow \mathbb{R}$}. We have a high
        degree of non-uniqueness of solutions of to the Euler---Lagrange
        equations.

  \item{These symmetries give conserved quantities that equal to zero.} \label{en:gauge}
\end{enumerate}

We usually use (\cref{en:gauge}) to distinguish \textit{gauge} symmetries from
\textit{physical} symmetries.

\subsubsection{Relativistic Hamiltonian}
The Hamiltonian comes from Noether's theorem from \textit{time translation}
symmetry,

\begin{equation*}
  q_s(t) = q(t+s)
\end{equation*}

and this is an example of a reparameterization with $\delta f = 1$, so we see
from the previous results that the Hamiltonian is zero.

\begin{equation*}
  H = 0,
\end{equation*}

which implies that there is no time evolution, so you know why people talk about
``the problem of time'' in general relativity. There is another conserved
quantity that has the title of ``energy'' which is not zero. It comes from the
symmetry,

\begin{align*}
  q_s(t) = q(t) + s\,w
\end{align*}

where $w\in \mathbb{R}^{n+1}$ and $w$ points in some time-like direction.

\begin{align*}
  \delta L &= \pdv{L}{q^\mu}\delta q^\mu + \pdv{L}{\dot{q}^\mu}\delta \dot{q}^\mu\\
  &= p_\mu \delta \dot{q}^\mu \quad \text{since $\pdv{L}{q^\mu}=0$ and $\pdv{L}{\dot{q}^\mu}=p_\mu$} \\
  &= p_\mu 0 = 0
\end{align*}

Thus, in fact, any $w$ gives a conserved quantity. We have $\delta q^i=w^i,
\delta \dot{q}^i = \dot{w}^i = 0$. We get a conserved quantity

\begin{equation*}
  p_\mu\delta q^\mu - \ell = p_\mu w^\mu
\end{equation*}

namely, the \textbf{momentum in the $w$ direction}. It comes from spacetime
translation symmetry:

\begin{align*}
  p = (p_0, p_1, \cdots, p_n)
\end{align*}

where $p_0$ is energy and $(p_1,\cdots,p_n)$ is spatial momentum.

\subsection{Relativistic Particles in an Electromagnetic Field}
\label{sec:re_p_emf}
The electromagnetic field is described by a one-form $A$ on spacetime, $A$ is
the \textbf{vector potential}, such that

\begin{equation}
  dA = F
\end{equation}

is a two-form containing the electric and magnetic fields,

\begin{equation}
  F_{\mu\nu} = \pdv{A_\nu}{x^\mu} - \pdv{A_\mu}{x^\nu},
\end{equation}

where $\pdv{}{x_\mu}=\partial_\mu=\partial/\partial(ct),\nabla$ and
$A_\mu=(\phi/c,-\bm{A})$. Thus, we have

\begin{align*}
F^{\mu\nu}&={\begin{bmatrix}0&-E_{x}/c&-E_{y}/c&-E_{z}/c\\E_{x}/c&0&-B_{z}&B_{y}\\E_{y}/c&B_{z}&0&-B_{x}\\E_{z}/c&-B_{y}&B_{x}&0\end{bmatrix}} \\
\displaystyle F_{\mu\nu }=\eta_{\mu \alpha }F^{\alpha\beta }\eta_{\beta\nu }&={\begin{bmatrix}0&E_{x}/c&E_{y}/c&E_{z}/c\\-E_{x}/c&0&-B_{z}&B_{y}\\-E_{y}/c&B_{z}&0&-B_{x}\\-E_{z}/c&-B_{y}&B_{x}&0\end{bmatrix}}.
\end{align*}

where $E$ is the electric field and $B$ is the magnetic field. The action for a
particle of charge $e$ is

\begin{equation*}
  S = m \underbrace{\int_{t_0}^{t_1} \norm{\dot{q}}\,dt}_\text{proper time} + e \underbrace{\int_{q}A}_\text{integral of $A$ along the path $q$}.
\end{equation*}

Note that since $A$ is a one-form we can integrate it over an oriented manifold,
but one can also write the path integral using time $t$ as a parameter, with the
differential $A_\mu\dot{q}^\mu dt$, after $dq^\mu=\dot{q}^\mu dt$.

The Lagrangian in the above action, for the charge $e$ with mass $m$ in an
electromagnetic potential $A$ is

\begin{equation} \label{eq:lag_emf}
  L(q,\dot{q}) = m\norm{\dot{q}} + e A_\mu \dot{q}^\mu
\end{equation}

so we can work out the Euler-Lagrange equation:

\begin{equation*}
  p_\mu = \pdv{L}{\dot{q}^\mu} = m \frac{\dot{q}_\mu}{\norm{\dot{q}}} + e A_\mu = mv_\mu + e A_\mu
\end{equation*}

where $v \in \mathbb{R}^{n+1}$ is the velocity, normalized so that $\norm{v} =
1$. Note that now \textit{momentum is no longer mass times velocity}! That is
because we are in $(n+1)$d space, the momentum is an $(n+1)$ vector. Continuing
the analysis, we find the force

\begin{align*}
  F_\mu = \pdv{L}{q^\mu} &= \pdv{}{q^\mu}\left(eA_\nu\dot{q}^\nu\right) \\
  &= e\pdv{A_\nu}{q^\mu}\dot{q}^{\nu}
\end{align*}

so we have

\begin{align*}
  \dot{p} &= F \\
  \frac{d}{dt}\left(m v_\mu + e A_\mu\right) &= e\pdv{A_\nu}{q^\mu}\dot{q}^\nu \\
  m\frac{v_\mu}{dt} &= e\pdv{A_\nu}{q^\mu}\dot{q}^\nu - e\frac{dA_\mu}{dt}\\
  m\frac{v_\mu}{dt} &= e\pdv{A_\nu}{q^\mu}\dot{q}^\nu - e\pdv{A_\mu}{q^\nu}\dot{q}^\nu\\
  &= e \left(\pdv{A_\nu}{q^\mu} - \pdv{A_\mu}{q^\nu}\right) \dot{q}^\nu = e \bm{F}_{\mu\nu} \dot{q}^\nu.
\end{align*}

We get the following equation of motion

\begin{equation}
  m\frac{dv_\mu}{dt} = e F_{\mu\nu}\dot{q}^\nu \quad \text{Lorentz force law}.
\end{equation}

\subsection{Lagrangian for a String}
We have looked at a point particle and tried

\begin{equation*}
  S = m\cdot \underbrace{\text{(arclength)}}_\text{proper time} + \overbrace{\int \underbrace{A}_\text{1-form}}^\text{integrate over a 1D path}.
\end{equation*}

In String theory we boost the dimension by $+1$ and consider a string tracing
out a 2D surface as time passed. The worldtube quantity analogous to arclength
or proper time would be the area of the worldtube. If the string is also assumed
to be a source of electromagnetic field then we need a 2-form to integrate over
the 2D worldtube analogous to the 1-form integrating over the path line of the
point particle. In string theory this is usually the ``Kalb-Ramond field'' $B$.
To recover electromagnetic interactions if should be anti-symmetric like $A$,
its tensor components will have two indices since it is a 2-form. The string
action can be written as

\begin{equation}
  S = \alpha \cdot \text{(area)} + e \int B
\end{equation}

We have also replace the point particle mass by the string tension $\alpha$,
which has the dimension [ $\frac{M}{L}$ ] to obtain the correct unit for the
action.

\section{Another Lagrangian for Relativistic Electrodynamics}
In \cref{sec:re_p_emf}, \cref{eq:lag_emf} we saw an example of Lagrangian for
relativistic electrodynamics that had awkward reparameterization symmetries,
namely $H=0$ and there were non-unique solutions to the Euler-Lagrange equation
arising from applying gauge transformations. This freedom to change the gauge
can be avoided. We can use the Lagrangian

\begin{equation}
  L = \frac{m}{2}\dot{q}\cdot \dot{q} + eA_\mu \dot{q}^\mu,
\end{equation}

which does not have gauge symmetry, and it yields

\begin{align}
  p_\mu &= \pdv{L}{\dot{q}^\mu} = m\dot{q}_\mu + e A_\mu \\
  F_\mu &= \pdv{L}{q^\mu} = e\pdv{A_\nu}{q^\mu}\dot{q}^\nu.
\end{align}

Thus, the equation of motion is

\begin{align}
  \frac{d}{dt} \left(m\dot{q}_\mu + e A_\mu\right) &=
  e\pdv{A_\nu}{q^\mu}\dot{q}^\nu \\
  m\ddot{q}_\mu = eF_{\mu\nu} \dot{q}^\nu.
\end{align}

The procedure to calculate the Hamiltonian is very straightforward at this
point

\begin{align}
  H &= p_\mu\dot{q}^\mu - L \\
  &= (m\dot{q}_\mu + e A_\mu) \dot{q}^\mu - \frac{m}{2}\dot{q}\cdot \dot{q} +
  eA_\mu \dot{q}^\mu \nonumber\\
  &= \frac{m}{2}m \dot{q}_\mu \dot{q}_\mu \ne 0.
\end{align}

\subsection{Free Particles in General Relativity}
In general relativity, spacetime is an $(n+1)$-dimensional Lorentzian manifold,
namely a smooth $(n+1)$-dimensional manifold $Q$ with a \textit{Lorentzian
metric $g$}. We define the metric as

\begin{enumerate}
  \item For every $x\in Q$, we have a bilinear map
    \begin{align*}
      g(x): T_xQ\times T_x Q \longrightarrow \mathbb{R} \\
      (v, w) \mapsto g(x)(v, w).
    \end{align*}
  \item We have
    \begin{align*}
      g_{\mu\nu} &= \begin{bmatrix}
        1&0&\cdots&0 \\ 0&-1&&0 \\ \vdots &&\ddots&\vdots \\ 0&0&\cdots&-1
      \end{bmatrix}\\
      g(v, w) &= g_{\mu\nu} v^\mu w^\nu.
    \end{align*}
  \item $g(x)$ is smooth.
\end{enumerate}

%TODO
