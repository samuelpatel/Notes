\section{From Lagrangian Mechanics to Hamiltonian Mechanics}
In the Lagrangian approach we focus on the \textit{position} and
\textit{velocity} of a particle, and compute how the particle behaves from the
Lagrangian $L(q,\dot{q})$, which is a function

\begin{equation*}
  L: TQ \longrightarrow \mathbb{R}
\end{equation*}

where the \textbf{tangent bundle} is the space of position-velocity pairs, but
we are led to consider the momentum

\begin{equation*}
  p_i = \pdv{L}{\dot{q}^i}
\end{equation*}

since the equations of motion tell us how it changes over time

\begin{equation*}
  \frac{dp_i}{dt} = \pdv{L}{q^i}.
\end{equation*}

\subsection{The Hamiltonian Approach}
In the Hamiltonian approach we work on the so called \textit{phase space}, and
study the behavior of the particle by computing the energy of the particle

\begin{align*}
  &H = p_i \dot{q}^i - L(q, \dot{q})\\
  &H: T^*Q \longrightarrow \mathbb{R}.
\end{align*}

The Hamiltonian is a function of position and momentum, and the
\textbf{cotangent bundle} is the space of position-momentum pairs which
satisfy \textbf{Hamilton's equations}:

\begin{equation}
  \dot{q}^i = \pdv{H}{p_i}, \quad \dot{p}^i = -\pdv{H}{q_i}
\end{equation}

where is the latter is essentially the Euler-Lagrange equation

\begin{equation}
  \dot{p}^i = \pdv{L}{q}_i.
\end{equation}

We have the map

\begin{align*}
  \lambda: TQ &\longrightarrow T^*Q\\
  (q,\dot{q}) &\mapsto (q, p)
\end{align*}

in the Hamiltonian description, where $q\in Q$, $\dot{q}$ is \textbf{any}
tangent vector in $T_qQ$, and $p$ is a cotangent vector in $T^*_qQ:=(T_qQ)^*$,
given by

\begin{equation}
  \dot{q} \stackrel{\lambda}{\longrightarrow} p_i = \pdv{L}{\dot{q}^i}.
\end{equation}

Thus, $\lambda$ is defined using $L: TQ\rightarrow\mathbb{R}$. We can view the
$\pdv{L}{\dot{q}^i}$ in a coordinate-free fashion, as is the differential of
$L$ in the vertical direction, i.e. the $\dot{q}^i$ directions. Note that the
differential of

\begin{align*}
  \pi: TQ &\longrightarrow Q\\
  (q, \dot{q}) &\mapsto q
\end{align*}

is map

\begin{equation*}
  d\pi: TTQ \longrightarrow TQ.
\end{equation*}

We define the bundle of \textbf{vertical vectors} to be

\begin{equation}
  VTQ = \{\; v \in TTQ: d\pi(v) = 0 \;\} \subseteq TTQ.
\end{equation}

That is, it consists of tangent vectors to $TQ$ that are sent to zero by
$d\pi$. The differential of $L$ at some point $(q,\dot{q})\in TQ$ is a map from
$TTQ$ to $\mathbb{R}$, so we have

\begin{equation}
  (dL)_{(q,\dot{q})} \in T^*_{(q,\dot{q})} T Q
\end{equation}

that is,

\begin{equation}
  dL_{(q,\dot{q})}: T_{(q,\dot{q})}TQ\longrightarrow \mathbb{R}.
\end{equation}

We can define

\begin{equation}
  f: V_{(q,\dot{q})}TQ \rightarrow \mathbb{R}.
\end{equation}

We know that $T_{(q,\dot{q})}T_qQ$ and $T_qQ$ are isomorphic in a canonical
way, and $V_{(q,\dot{q})}(T_qQ) \cong T_{(q,\dot{q})}T_qQ$. So $f$ offers a
linear map

\begin{equation}
  p: T_qQ \longrightarrow \mathbb{R}
\end{equation}

that is,

\begin{equation}
  p\in T^*_qQ
\end{equation}

which is the momentum. Give $L: TQ\rightarrow T^*Q$, we now know a
coordinate-free way of describing the map

\begin{align*}
  \lambda: TQ &\longrightarrow T^*Q \\
  (q, \dot{q}) &\mapsto (q, p)
\end{align*}

given in local coordinates by

\begin{equation*}
  p_i = \pdv{L}{\dot{q}^i}.
\end{equation*}

If $\lambda$ is a diffeomorphism from $TQ$ to some open subset $X\subseteq
T^*Q$, then $L$ is \textbf{regular}. In this case, we can describe the behavior
of our system equally well by specifying position and velocity $(q,\dot{q})\in
TQ$ or position and momentum $(q,p) = \lambda(q,\dot{q})\in X$, where $X$ is
the \textbf{phase space} of the system, and we often have $X=T^*Q$, then $L$ is
said to be \textbf{strongly regular}.

\subsection{Hamilton's Equations}
Now assume $L$ is regular, so the map

\begin{align*}
  \lambda: TQ &\longrightarrow X \subseteq T^*Q \\
  (q, \dot{q}) &\mapsto (q, p)
\end{align*}

is a diffeomorphism. This let us have the beast of both worlds: we can identify
$TQ$ with $X$ by using $\lambda$, thus we can treat $q^i, p^i, L, H$ as
functions on $X$ (or $TQ$). In particular

\begin{equation*}
  \dot{q}_i := \pdv{L}{q^i} \quad \text{Euler-Lagrange equation}
\end{equation*}

which is really a function on $TQ$, but we will treat it as a function on $X$.
Now we have

\begin{align*}
  dL &= \pdv{L}{q^i}dq^i + \pdv{L}{\dot{q}^i}d\dot{q}^i \\
  &= \dot{p}_idq^i + p_id\dot{q}^i
\end{align*}

while

\begin{align*}
  dH &= d(p_i\dot{q}^i - L) \\
  &= \dot{q}^idq_i + p_id\dot{q}^i - dL \\
  &= \dot{q}^idq_i + p_id\dot{q}^i - (\dot{p}_idq^i + p_id\dot{q}^i) \\
  &= \dot{q}^idp_i - \dot{p}_id\dot{q}^i
\end{align*}

Together, we have

\begin{align}
  dL &= \dot{p}_idq^i + p_id\dot{q}^i \\
  dH &= \dot{q}^idp_i - \dot{p}_id\dot{q}^i
\end{align}

Using the local coordinates $(q^i, p_i)$, we can work out that

\begin{equation}
  dH = \pdv{H}{p^i} dp_i + \pdv{H}{q^i} dq^i,
\end{equation}

since $dp_i, dq^i$ form a basis of 1-forms, we conclude that

\begin{equation}
  \dot{q}^i = \pdv{H}{p_i},\quad \dot{p}_i = -\pdv{H}{q_i}.
\end{equation}

These are the \textbf{Hamilton's Equations}.

\subsection{Hamilton and Euler-Lagrange}
Though $\dot{q}^i$ and $\dot{p}_i$ are just functions of $X$, when the
Euler-Lagrange equations hold for some path $q: [t_0, t_1]\rightarrow Q$, they
will be the time derivatives of $q^i$ and $p_i$. So when the Euler-Lagrange
equation holds, the Hamiltonian description of the system is just the
Euler-Lagrange equation. The equation

\begin{equation*}
  \dot{q}^i = \pdv{H}{p_i}
\end{equation*}

is a way to recover the velocity $\dot{q}$ from $q$ and $p$, and its inverse is

\begin{equation*}
  p_i = \pdv{L}{\dot{q}^i}
\end{equation*}

which calculates $p$ from $q$ and $\dot{q}$, thus we have the map

\begin{align}
  \lambda^{-1}: X &\longrightarrow TQ \\
  (q,p) &\mapsto (q, \dot{q}).
\end{align}

The other Hamilton's equation is just the Euler-Lagrange equation. Note that we
have

\begin{align*}
  \dot{p}_i &= \frac{d}{dt}\pdv{L}{\dot{q}^i} \\
  -\pdv{H}{q^i} &= -\pdv{}{q^i} \left(p_i\dot{q}^i - L\right) = \pdv{L}{q^i},
\end{align*}

thus,

\begin{align*}
  \frac{d}{dt}\pdv{L}{\dot{q}^i} &= \pdv{L}{q^i} \\
  \dot{p}_i &= -\pdv{H}{q^i}.
\end{align*}

\paragraph{Example: A Particle in a Potential $V(q)$:}
For a particle in $Q=\mathbb{R}^n$ in a potential $V: \mathbb{R}^n\rightarrow
\mathbb{R}$ the system has the Lagrangian

\begin{equation*}
  L(q, \dot{q}) = \frac{m}{2}\norm{\dot{q}}^2 - V(q)
\end{equation*}

which gives

\begin{equation*}
  p_i = m \dot{q}^i, \quad \dot{q}^i = \frac{g^{ij}p_j}{m}
\end{equation*}

and the Hamiltonian

\begin{equation*}
  H(q,p) = p_i\dot{q}^i - L = \frac{\norm{p}^2}{m} -
  \left(\frac{\norm{p}^2}{2m} - V(q) \right) = \frac{\norm{p}^2}{2m} + V(q).
\end{equation*}

Thus, we have

\begin{align*}
  \dot{q}^i = \pdv{H}{p_i} &\implies \dot{q}^i = \frac{g^{ij}p_j}{m} \\
  \dot{p}_i =-\pdv{H}{q^i} &\implies \dot{p} = -\nabla V
\end{align*}

The first equation just recovers $\dot{q}$ as a function of $p$, where as the
second equation is $F=ma$.

\subsection{Hamilton's Equations from the Principle of Least Action}
We can get Hamilton's equations \textit{directly} by assigning an action $S$ to
any time evolution $x: [t_0, t_1] \rightarrow X$ and solve for $\delta S = 0$.
By ``\textit{directly}'', we will not impose any relation between $p$ and
$q,\dot{q}$.

Let $P$ be the space of time evolutions in the phase space $X$ and define the
action

\begin{align}
  S: P &\longrightarrow \mathbb{R}\\
  S(x) &= \int_{t_0}^{t_1} p_i\dot{q}^i - H \;dt
\end{align}

since $p_i\dot{q}^i - H$ is another way of writing the Lagrangian. So we have

\begin{align*}
  \delta S &= \delta \int p_i\dot{q}^i - H \;dt \\
  &= \int \delta p_i\dot{q}^i + p_i\delta\dot{q}^i - \delta H \;dt \\
  &= \int \delta p_i\dot{q}^i \underbrace{ - \dot{p}_i\delta
  q^i}_\text{integration by parts} - \delta H \;dt \\
  &= \int \delta p_i\dot{q}^i - \dot{p}_i\delta q^i - \pdv{H}{q^i}\delta q^i -
  \pdv{H}{p_i}\delta p_i \;dt \\
  &= \int \delta p_i (\dot{q}^i - \pdv{H}{p_i}) + \delta q^i (-\dot{p}_i -
  \pdv{H}{q^i}) \;dt
\end{align*}

This vanishes $\forall \delta x = (\delta q, \delta p)$ if and only if
Hamilton's equations

\begin{equation*}
  \dot{q}^i = \pdv{H}{p_i}, \quad p_i = -\pdv{H}{q^i}
\end{equation*}

hold.

To summarize, we have seen principle of least action in two differential
manifolds:

\begin{itemize}
  \item For paths in the configuration space $Q, \delta S = 0 \Longleftrightarrow$
    Euler-Lagrange equation.
  \item For paths in the phase space $X, \delta S = 0 \Longleftrightarrow$
    Hamilton's equations.
\end{itemize}

We can also apply the principle of least action in the \textbf{extended phase
space} where we have an additional coordinate for time: $X \times \mathbb{R}$,
so we have the action

\begin{equation} \label{eq:extended_phase}
  S(x) = \int p_i\frac{q^i}{dt} - H\; dt = \int p_idq^i - H \;dt.
\end{equation}

We can interpret the integrand as a 1-form $\beta = p_idq^i - H$ on the space
$X\times \mathbb{R}$, which has coordinates ${p_i, q^i, t}$, so have have the
path

\begin{align}
  \sigma: [t_0, t_1] &\longrightarrow X\times \mathbb{R} \\
  t \mapsto (x(t), t)
\end{align}

where $x: [t_0, t_1] \rightarrow X$, and the action becomes the integral of a
1-form over a curve:

\begin{equation}
  S(x) = \int p_idq^i - H \;dt = \int_\sigma \beta.
\end{equation}

\subsection{Waves versus Particles---The Hamilton-Jacobi Equations}
In quantum mechanics we know that every particle is a wave, and vice versa. We
can approximate light as particles moving along geodesics, and here we start
with a Riemannian manifold $(Q, g)$ and a new metric

\begin{equation}
  h_{ij} = n^2g_{ij}
\end{equation}

where $n: Q\rightarrow (0, \infty)$ is the index of refraction throughout
space.

\subsubsection{Wave Equations}
Huygens considered the motion of a wavefront, and discovered that the wavefront
is the envelope of a bunch of little wavelets centered at points along the old
wavefront. In short, the wavefront moves at unit speed in the normal direction
with respect to the ``optical metric'' $h$. We can think of the distance
function as

\begin{equation}
  d: Q\times Q \longrightarrow [0, \infty)
\end{equation}

on the Riemannian manifold $(Q, h)$, where

\begin{equation}
  d(q_0, q_1) = \inf_{\Pi} (\text{arclength}).
\end{equation}

This $d(q_0, q_1)$ is the least action---the infimum of action over all paths
from $q_0$ to $q_1$. Using this we get the wavefront centered at $q_0 \in Q$ as
the level sets

\begin{equation}
  \{ q | d(q_0, q) = c \}
\end{equation}

or at least for small $c > 0$. We can approximate the waves of the light by the
wavefunction:

\begin{equation}
  \phi(q) = A(q) e^{ik\;d(q_0, q)}
\end{equation}

where $k$ is the wavenumber of the light and $A: Q\rightarrow \mathbb{R}$
describes the amplitude of the wave, which drops off far from $q_0$. This is
the so called \textbf{eikonal approximation}. Hamilton and Jacobi focused on
the distance $d: Q\times Q \rightarrow [0, \infty]$ as function of \textit{two
variables}, which is now called \textit{Hamilton's principal function}, and we
will denote it as $W$. We have

\begin{equation}
  \pdv{}{q_1^i} W(q_0, q_1) = (p_1)_i
\end{equation}

where $p_1$ is a \textit{cotangent vector normal to the wavefronts}.

\subsubsection{The Hamilton-Jacobi Equations}
If the level sets remain smooth, we can describe wavefronts by them. In the
eikonal approximation, light is described by waves

\begin{align}
  \phi: Q &\longrightarrow \mathbb{C} \\
  \phi(q_1) &= A(q_1) e^{ik\; W(q_0, q_1)}
\end{align}

and we have

\begin{equation}
  W(q_0, q_1) = \inf_{q\in \Pi} S(q)
\end{equation}

where $\Pi$ is the space of paths from $q_0$ to $q$, and $S(q)$ is the action
of the path $q$, its arclength. We also saw that

\begin{equation*}
  \pdv{}{q_1^i} W(q_0, q_1) = (p_1)_i
\end{equation*}

where the tangent vectors point to the

\begin{equation}
  p_1^i = h^{ij} (p_1)_j
\end{equation}

direction. In fact, $kp_1$ is the \textit{momentum} of the light passing
through $q_1$. This foreshadows quantum mechanics. In quantum mechanics, the
momentum is an operator that acts to differentiating the wavefunction.

Jacobi generalized this to the motion of point particles in a potential $V:
Q\rightarrow \mathbb{R}$, using the fact that a particle of energy $E$ traces
out geodesics in the space with the metric

\begin{equation}
  h_{ij} = \frac{2(E-V)}{m} g_{ij}.
\end{equation}

Suppose $Q$ is any manifold and $L: TQ\rightarrow \mathbb{R}$ is any
Lagrangian. We can define Hamilton's principal function

\begin{align}
  W: Q\times \mathbb{R} \times Q \times \mathbb{R} &\longrightarrow \mathbb{R}
  \\
  W(q_0, t_0; q_1, t_1) &= \inf_{q\in\Pi} S(q)
\end{align}

where

\begin{align}
  \Pi &= \{ q: [t_0, t_1] \rightarrow Q, q(t_0) = q_0, q(t_1) = q_1 \} \\
  S(q)&= \int_{t_0}^{t_1} L\left( q(t), \dot{q}(t) \right)
\end{align}

$W$ is just \textit{least action} for a path from $(q_0, t_0)$ to $(q_1, t_1)$,
and it will be smooth if those two points is close enough. In fact, we have

\begin{align}
  \pdv{W}{q_1^i} &= (p_1)_i, \\
  \pdv{W}{q_0^i} &= -(p_0)_i, \\
  \pdv{W}{t_1} &= -H_1, \\
  \pdv{W}{t_0} &= H_0
\end{align}

which are the \textbf{Hamilton-Jacobi equations}. The mysterious minus sign in
front of energy was seen before in the 1-form,

\begin{equation*}
  \beta = p_idq^i - H \; dt
\end{equation*}

on the extended phase space $X \times \mathbb{R}$. Given $(q_0, t_0), (q_1,
t_1)$, let

\begin{equation*}
  q: [t_0, t_1] \longrightarrow Q
\end{equation*}

be the action-minimizing path from $q_0$ to $q_1$. Then, we can vary $q_0$ and
$q_1$ a bit, but note that the variation $\delta q$ does not vanish at the
boundary. We have

\begin{align*}
  \delta W &= \delta S \\
  &= \delta \int_{t_0}^{t_1} L(q, \dot{q})\; dt \\
  &= \int_{t_0}^{t_1} \pdv{L}{q^i}\delta q^i + \pdv{L}{\dot{q}^i}\delta
  \dot{q}^i\; dt \\
  &= \int_{t_0}^{t_1} \pdv{L}{q^i}\delta q^i \underbrace{- \dot{p}_i
  \delta q^i\; dt + p_i\delta q^i \bigg\vert_{t_0}^{t_1}}_\text{integration by
  parts} \\
  &= \underbrace{
       \int_{t_0}^{t_1} \left(
       \overbrace{\pdv{L}{q^i} - \dot{p}_i}^\text{Euler-Lagrange}
       \right)\delta q^i\; dt
     }_\mathbf{0}
     + p_i\delta q^i \bigg\vert_{t_0}^{t_1}
\end{align*}

because $q$ minimizes the action, thus the term inside the parentheses is zero,
and it satisfies the \textit{Euler-Lagrange equation}. We have

\begin{equation} \label{eq:HJE1}
  \delta W = p_{1i}\delta q_1^i - p_{0i}\delta q^i_0.
\end{equation}

and so

\begin{equation}
  \pdv{W}{q_1^i} = p_{1i}, \quad \pdv{W}{q_0^i} = -p_{0i}
\end{equation}

We can also vary $t_0$ and $t_1$. The Lagrangian $L: TQ\rightarrow \mathbb{R}$
is assumed to be \textit{regular}, so that we have

\begin{align*}
  \lambda TQ: &\longrightarrow X \subseteq T^*Q \\
  (q, \dot{q}) &\mapsto (q, p)
\end{align*}

is a diffeomorphism. We have to ensure that $(q_0, t_0)$ is close enough to
$(q_1, t_1)$ that there is a unique $q\in \Pi$ that minimizes the action, and
assume that $q$ depends smoothly on $u = (q_0, t_0; q_1, t_1) \in (Q\times
\mathbb{R})^2$. Then the Hamilton's principal function is

\begin{equation*}
  W(u) := W(q_0, t_0; q_1, t_1) = \int_{t_0}^{t_1} p \; dq
  - H \; dt = \int_{C} \beta
\end{equation*}

recall that $\beta$ is a 1-form on the extended phase space $X\times
\mathbb{R}$, and $C$ is a curve in the extended phase space:

\begin{equation}
  C(t) = \left(q(t), p(t), t\right) \in X \times \mathbb{R}
\end{equation}

in \cref{eq:extended_phase}. Note that $C$ depends on the curve $q\in \Pi$
which in ture depends on $u\in (Q\times \mathbb{R})^2$. We can take
the derivative of $W$ respect to $u$ and get the Hamiltonian Jacobi equations
from $\beta$. Let $u_s$ be a 1-parameter family of points in $(Q\times
\mathbb{R})^2$:

\begin{equation*}
  \frac{d}{ds} W(u_s) = \frac{d}{ds}\int_{C_s} \beta.
\end{equation*}

We can make two other paths $A_s$, $B_s$ such that $A_s + B_s + C_s$ has the
same end-points with $C_0$. Although the sum is not smoth, we can always
approximate them to be smooth. Thus, we have

\begin{align*}
  \frac{d}{ds}\int_{A_s + B_s + C_s} \beta &= 0 \\
  \frac{d}{ds}\int_{C_s} \beta &= \frac{d}{ds} \int_{B_s} \beta -
  \frac{d}{ds}\int_{A_s} \beta \text{ at } s=0
\end{align*}

Note that we have

\begin{equation*}
  \frac{d}{ds}\int_{A_s} \beta = \frac{d}{ds} \int \beta (A'_r)\; dr =
  \beta(A'_0)
\end{equation*}

where $A'_0 = v$ is the tangent vector of $A_s$ at $s=0$, similarly, we have
$\frac{d}{ds}\int_{B_s}\beta = \beta(w)$. So,

\begin{equation}
  \frac{d}{ds}W(u_s) = \beta(w) - \beta(v)
\end{equation}

where $w$ is the change of $(q_1, p_1, t_1)$ as we perturbate $C$ and $v$ is
the change of $(q_0, p_0, t_0)$. Since we know that $\beta = p^idq_i - H\; dt$,
we get

\begin{align*}
  \pdv{W}{q_1^i} &= p_1^i \\
  \pdv{W}{t_1} &= -H \\
  \pdv{W}{t_0} &= H \\
  \pdv{W}{q_0^i} &= -p_0^i.
\end{align*}

We are only interested in the wavefront and $H$ depends on $(q, p, t)$, thus
the Hamilton-Jacobi equation is just

\begin{equation}
  \pdv{W}{t} = -H(q^i, \pdv{W}{q^i}, t).
\end{equation}
